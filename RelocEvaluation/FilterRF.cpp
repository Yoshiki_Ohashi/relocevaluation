#include "FilterRF.h"
using namespace cv;


FilterRF::FilterRF() : Filter() {

}

FilterRF::FilterRF(int thread) : Filter(thread) {

}

FilterRF::FilterRF(Method method) : Filter(method) {

}

FilterRF::FilterRF(int thread, Method method) : Filter(thread, method) {

}

FilterRF::FilterRF(Size size) : Filter(size) {

}

FilterRF::FilterRF(int thread, Size size) : Filter(thread, size) {

}

FilterRF::FilterRF(Method method, Size size) : Filter(method, size) {

}

FilterRF::FilterRF(int thread, Method method, Size size) : Filter(thread, method, size) {

}

FilterRF::FilterRF(filter filter) : Filter(filter) {

}

FilterRF::FilterRF(int thread, filter filter) : Filter(thread, filter) {

}

FilterRF::FilterRF(ImageIO* imgIO) : Filter(imgIO) {

}

FilterRF::FilterRF(ImageIO* imgIO, int thread) : Filter(imgIO, thread) {

}

FilterRF::FilterRF(ImageIO* imgIO, Method method) : Filter(imgIO, method) {

}

FilterRF::FilterRF(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method) {

}

FilterRF::FilterRF(ImageIO* imgIO, Size size) : Filter(imgIO, size) {

}

FilterRF::FilterRF(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size) {

}

FilterRF::FilterRF(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size) {

}

FilterRF::FilterRF(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size) {

}

FilterRF::FilterRF(ImageIO* imgIO, filter filter) : Filter(imgIO, filter) {

}

FilterRF::FilterRF(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter) {

}

FilterRF::~FilterRF()
{
	delete[] positions;
	delete[] thresholds;
	delete[] pPositions;
	delete[] tables;
	for (int i = 0; i < filledDB; ++i) {
		delete[] descriptors[i];
	}
}

bool FilterRF::checkSpeed(int repeat) {
	if (!SpeedIsChecked) {
		randomizePosAndThresh();		
		init();
		random_device rnd;
		uniform_int_distribution<> random(0, DBsize - 1);
		makeDB(repeat);
		timePrepare = 0;
		timeCompare = 0;
		for (int i = 0; i < repeat; i++) {
			ImgIO->read(random(rnd));
			screening(repeat, &timePrepare, &timeCompare);
		}
		clearDB();
		timePrepare = timePrepare * 1000 / (double)repeat;
		timeCompare = timeCompare * 1000 / (double)repeat / (double)repeat;
		SpeedIsChecked = true;
	}
	double time = timePrepare + timeCompare*candidateSize;
	if (time < requiredTime) return true;
	else return false;
}

void FilterRF::makeDB(){
	randomizePosAndThresh();
	allocMat();
	getPointers();
	hasToCount = false;
	for (int ID = 0; ID < DBsize; ID++) {
		extractFeature(ID);
		addIDtoTable(ID);
	}
}

void FilterRF::makeDB(int repeat) {
	random_device rnd;
	uniform_int_distribution<> random16(0, 15);
	int code;
	for (int i = 0; i < DBsize; ++i) {
		for (int j = 0; j < numFerns; ++j) {
			code = random16(rnd);
			tables[16 * j + code].push_back(i);
		}
	}
}

void FilterRF::randomizePosAndThresh() {
	int n = size.width * size.height * 3;
	vector<int> pixels;
	generateUniqueRandomArray(pixels, 4 * numFerns, 0, n - 1);
	for (int i = 0; i < 4 * numFerns; ++i) {
		positions[i] = pixels[i];
	}

	thresholds = new uchar[4 * numFerns];
	random_device rnd;
	uniform_int_distribution<> rand256(0, 255);
	for (int i = 0; i < 4 * numFerns; ++i) {
		thresholds[i] = (uchar)rand256(rnd);
	}
}

void FilterRF::allocMat() {
	blurred = Mat::zeros(size, CV_32FC3);
}

void FilterRF::getPointers() {
	//float* data = (float*)blurred.data;
	for (int i = 0; i < 4 * numFerns; ++i) {
		if(i > -1) pPositions[i] = (float*)blurred.data + positions[i];
	}
}

void FilterRF::calcFeature() {
	filter2D(ImgIO->getResizedRGB(thread), blurred, CV_32F, kernel);
	for (int i = 0; i < numFerns; ++i) {
		descriptor[i] = 0;
		for (int j = 0; j < 4; ++j) {
			if (*(pPositions[4 * i + j]) < thresholds[4 * i + j]) {
				descriptor[i] |= 1 << j;
			}
		}
	}
	if (hasToCount) countCooccurrence();
}

void FilterRF::addIDtoTable(int ID) {
	for (int i = 0; i < numFerns; ++i) {
		tables[16 * i + descriptor[i]].push_back(ID);
	}
}

void FilterRF::addFeatureToDB(){
	descriptors[filledDB] = new uchar[numFerns];
	memcpy(descriptors[filledDB], descriptor, numFerns * sizeof(uchar));
	filledDB++;
}

void FilterRF::countCooccurrence() {
	for (int i = 0; i < DBsize; ++i) {
		coocurrence[i] = 0;
	}

	for (int i = 0; i < numFerns; ++i) {
		int tbl = 16 * i + descriptor[i];
		int jmax = tables[tbl].size();
		for (int j = 0; j < jmax; ++j) {
			coocurrence[(tables[tbl])[j]] += 1;
		}
	}
}

void FilterRF::generateUniqueRandomArray(vector<int>& vec, const int numElements, const int rangeMIN, const int rangeMAX) {
	int margin = 1.2 * numElements;
	vec.clear();
	vec.reserve(margin);
	random_device rand;
	uniform_int_distribution<> distribution(rangeMIN, rangeMAX);
	while (vec.size() < numElements) {
		while (vec.size() < margin) vec.push_back(distribution(rand));
		std::sort(vec.begin(), vec.end());
		auto unique_end = std::unique(vec.begin(), vec.end());

		if (numElements < std::distance(vec.begin(), unique_end)) {
			unique_end = std::next(vec.begin(), numElements);
		}
		vec.erase(unique_end, vec.end());
	}
	for (int i = 0; i < numElements - 1; ++i) {
		uniform_int_distribution<> dist(i, numElements - 1);
		int rnd = dist(rand);
		int tmp = vec[i];
		vec[i] = vec[rnd];
		vec[rnd] = tmp;
	}
}

void FilterRF::init() {
	allocMat();
	getPointers();
	hasToCount = true;
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterRF)
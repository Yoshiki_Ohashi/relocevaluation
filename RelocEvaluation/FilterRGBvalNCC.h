#pragma once
#include "Filter.h"
#include <vector>

extern double NCC(const cv::Mat img1, const cv::Mat img2);

class FilterRGBvalNCC :
	public Filter
{
public:
	FilterRGBvalNCC();
	FilterRGBvalNCC(int);
	FilterRGBvalNCC(Method);
	FilterRGBvalNCC(int, Method);
	FilterRGBvalNCC(cv::Size);
	FilterRGBvalNCC(int, cv::Size);
	FilterRGBvalNCC(Method, cv::Size);
	FilterRGBvalNCC(int, Method, cv::Size);
	FilterRGBvalNCC(filter);
	FilterRGBvalNCC(int, filter);
	FilterRGBvalNCC(ImageIO* imgIO);
	FilterRGBvalNCC(ImageIO* imgIO, int);
	FilterRGBvalNCC(ImageIO* imgIO, Method);
	FilterRGBvalNCC(ImageIO* imgIO, int, Method);
	FilterRGBvalNCC(ImageIO* imgIO, cv::Size);
	FilterRGBvalNCC(ImageIO* imgIO, int, cv::Size);
	FilterRGBvalNCC(ImageIO* imgIO, Method, cv::Size);
	FilterRGBvalNCC(ImageIO* imgIO, int, Method, cv::Size);
	FilterRGBvalNCC(ImageIO* imgIO, filter);
	FilterRGBvalNCC(ImageIO* imgIO, int, filter);
	~FilterRGBvalNCC();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	void clearDB(){
		descriptors.clear();
	}
	vector<cv::Mat> descriptors;
	void extractFeature(int ID){
		extractFeatureFromRGB(ID);
	}
	void extractFeature(){
		extractFeatureFromRGB();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return 1.0 - NCC(descriptor, descriptors[ID]);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterRGBvalNCC)
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/ximgproc.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include "Linker.h"
#include "define.h"

#include "ImageIO.h"
#include "Filter.h"
#include "FilterGRAYval.h"
#include "FilterRGBval.h"
#include "FilterGRAYvalNCC.h"
#include "FilterRGBvalNCC.h"
#include "FilterGRAYhist.h"
#include "FilterRGBhist.h"
#include "FilterLBPhist.h"
#include "FilterHOG.h"
#include "FilterCCV.h"
#include "FilterGabor.h"
#include "FilterSURF.h"
#include "FilterAKAZE.h"
#include "FilterSURFmatch.h"
#include "FilterAKAZEmatch.h"
#include "FilterLSDnumber.h"
#include "FilterLSDlength.h"
#include "FilterLSDregion.h"
#include "FilterLSDparallel.h"
#include "FilterLSDdistance.h"
#include "FilterLSDangle.h"
#include "FilterLSDsolo.h"
#include "FilterLSDcombi.h"
#include "FilterRF.h"
#include "FilterRForiginal.h"
#include "FilterRFwithout.h"

using namespace std;
using namespace cv;
namespace fs = boost::filesystem;

double NCC(Mat, Mat);
void MakeFilterObject(Filter**, filter);

int main() {
#ifdef Fugen
	const string DBpath = "C:\\kinectv2\\20170929125156\\";	//DB画像のディレクトリ
	const string FILEpath = "C:\\kinectv2\\20170929125156\\";	//入出力ファイルのディレクトリ
	const vector<string> TESTpaths = { "C:\\kinectv2\\201712_set1\\", "C:\\kinectv2\\201712_set2\\", "C:\\kinectv2\\201712_set3\\", "C:\\kinectv2\\201712_set4\\", "C:\\kinectv2\\201712_set5\\", "C:\\kinectv2\\201712_set6\\" };
	const vector<int> testSizes = { 2329,2363,2346,2150,1322,1971 };
#endif

#ifdef Seven_Scenes
	const string DBpath = "C:\\7-scenes\\DB\\";	//DB画像のディレクトリ
	const string FILEpath = "C:\\7-scenes\\";	//入出力ファイルのディレクトリ
	const vector<string> TESTpaths = { "C:\\7-scenes\\TEST\\" };
	const vector<int> testSizes = { 7799 };
#endif


	//疑似クエリ画像に対する正解画像の決定

	#ifdef Seven_Scenes
	cout << "loadding the answer lists for query images...";
	char filename[128] = "answers_TEST_20cm_20deg.txt";

	ifstream fin(FILEpath + filename);
	vector<vector<int>> answers;
	vector<int> answer;

	string str;
	while (getline(fin, str, '\t')) {
		answer.push_back(stoi(str));
		getline(fin, str, '\t');
		int size = stoi(str);
		if (size > 0) {
			for (int i = 0; i < size; ++i) {
				getline(fin, str, '\t');
				answer.push_back(stoi(str));
			}
			answers.push_back(answer);
		}
		getline(fin, str, '\n');

		answer.clear();
	}
	fin.close();
	fin.clear();

	cout << "done" << endl;
	cout << "answers.size() = " << answers.size() << endl << endl;
	#endif

	filter filters[NumThreads];
	Filter* pFilter[NumThreads];
	for (int th = 0; th <= NumThreads; ++th) pFilter[th] = NULL;

	ofstream resout(FILEpath + "evaluation_results.tsv");
	resout << "1st Filter Method\t1st Filter Size\t2nd Filter Method\t2nd Filter Size\t3rd Filter Method\t3rd Filter Size\tData Set\tSuccess Ratio\tAvg Answering Time\t\n";

	for (int num = 1; num < NumSelect; ++num) {		
	
		cout << "Loading DBs Num:" << num+1 << "...";

		char filename[64];
		ifstream fin;
		ImageIO QUERYimgIO;

		for (int thread = NumThreads; thread > 0; --thread) {
			int Num = (thread == NumThreads) ? num + 1 : filters[thread].NumPrevious + 1;
			sprintf_s(filename, "optimumFilter%d-%d_params.txt", thread, Num);
			fin.open(FILEpath + string(filename), std::ios::in | std::ios::binary);
			boost::archive::binary_iarchive ia(fin);
			ia >> filters[thread - 1];
			fin.close();
			fin.clear();
		}

		for (int thread = NumThreads; thread > 0; --thread) {
			sprintf_s(filename, "%s_%s_DB.txt", nameMethod(filters[thread - 1].method).c_str(), nameSize(filters[thread - 1].size).c_str());
			fin.open(FILEpath + string(filename), std::ios::in | std::ios::binary);
			boost::archive::binary_iarchive ia(fin);
			ia >> pFilter[thread - 1];
			*pFilter[thread - 1] = filters[thread - 1];
			pFilter[thread - 1]->setThreadNum(thread - 1);
			pFilter[thread - 1]->setImageIO(&QUERYimgIO);
			pFilter[thread - 1]->init();
			fin.close();
			fin.clear();
		}

		cout << "done" << endl;

		for (int set = 0; set < (int)TESTpaths.size(); ++set) {

			cout << " Testing \"" << TESTpaths[set] << "\" test set...";

			QUERYimgIO.setDBpath(TESTpaths[set]);
			ImageIO DBimgIO(DBpath);

			sprintf_s(filename, "%sresult%d.txt", TESTpaths[set].c_str(), num + 1);
			ofstream fout(filename);

			#ifdef Fugen
			fout << "QueryID\tSelectedID1\tS/F\tsimilarity\tSelectedID2\tS/F\tsimilarity\tSelectedID3\tS/F\tsimilarity\tSelectedID4\tS/F\tsimilarity\tSelectedID5\tS/F\tsimilarity\tSelectedID6\tS/F\tsimilarity\tSelectedID7\tS/F\tsimilarity\tSelectedID8\tS/F\tsimilarity\tSelectedID9\tS/F\tsimilarity\tSelectedID10\tS/F\tsimilarity" << endl;
			#endif

			#ifdef Seven_Scenes
			fout << "QueryID\tSelectedID1\tS/F\tSelectedID2\tS/F\tSelectedID3\tS/F\tSelectedID4\tS/F\tSelectedID5\tS/F\tSelectedID6\tS/F\tSelectedID7\tS/F\tSelectedID8\tS/F\tSelectedID9\tS/F\tSelectedID10\tS/F" << endl;
			#endif

			double screeningTime = 0;
			double successRatio = 0;
			LARGE_INTEGER freq, start, end;
			QueryPerformanceFrequency(&freq);

			for (int i = 0; i < testSizes[set]; ++i) {
				for (int th = 0; th < NumThreads; ++th) pFilter[th]->isFinished = false;
				pFilter[0]->isFinished = false;
				vector<int>* list;
				list = new vector<int>[NumThreads];
				for (int th = 0; th < NumThreads; ++th) list[th].reserve(int(DBsize / pow(CuttingRatio, th))+1);
				for (int id = 0; id < DBsize; ++id) list[0].push_back(id);
				random_device seedgen;
				mt19937_64 rnd(seedgen());
				shuffle(list[0].begin(), list[0].end(), rnd);

				vector<int> res;
				res.reserve(int(DBsize / pow(CuttingRatio, NumThreads)) + 1);

				#ifdef Fugen
				QUERYimgIO.read(i);
				#endif

				#ifdef Seven_Scenes
				QUERYimgIO.read(answers[i][0]);
				#endif

				boost::thread_group threads;

				QueryPerformanceCounter(&start);

				for (int th = 0; th < NumThreads; ++th) {
					if ((th == NumThreads - 1) && (th == 0)) threads.create_thread(boost::bind(&Filter::screeningFL, pFilter[th], &list[th], &res));
					else if (th == 0) threads.create_thread(boost::bind(&Filter::screeningF, pFilter[th], &list[th], &list[th + 1]));
					else if (th == NumThreads - 1) threads.create_thread(boost::bind(&Filter::screeningL, pFilter[th], &list[th], &res, &pFilter[th - 1]->isFinished));
					else threads.create_thread(boost::bind(&Filter::screeningN, pFilter[th], &list[th], &list[th + 1], &pFilter[th - 1]->isFinished));
				}

				threads.join_all();

				QueryPerformanceCounter(&end);
				screeningTime += (double)(end.QuadPart - start.QuadPart) / freq.QuadPart;

				int success = 0;

				#ifdef Fugen
				fout << i;
				Mat query = QUERYimgIO.getRGB();
				for (int j = 0; j < int(res.size()); ++j) {
					fout << '\t' << res[j];
					DBimgIO.read(res[j]);
					Mat ans = DBimgIO.getRGB();
					double similarity = NCC(query, ans);
					if (similarity > 0.90) {
						success = 1;
						fout << "\tS";
					}
					else fout << "\tF";
					fout << '\t' << similarity;
				}
				fout << '\t' << ((success == 1) ? "S" : "F") << '\t' << (double)(end.QuadPart - start.QuadPart) / freq.QuadPart << '\t';
				successRatio += success;
				#endif

				#ifdef Seven_Scenes
				fout << answers[i][0];
				Mat query = QUERYimgIO.getRGB();
				for (int j = 0; j < int(res.size()); ++j) {
					fout << '\t' << res[j];
					if (find(answers[i].begin() + 1, answers[i].end(), res[j]) != answers[i].end()) {
						success = 1;
						fout << "\tS";
					}
					else fout << "\tF";
				}
				fout << '\t' << ((success == 1) ? "S" : "F") << '\t' << (double)(end.QuadPart - start.QuadPart) / freq.QuadPart << '\t';
				successRatio += success;
				#endif

				fout << endl;
				fout.flush();
			}

			successRatio /= testSizes[set];
			screeningTime /= testSizes[set];

			fout << endl << endl;
			fout << "successRatio : " << successRatio << endl;
			fout << "screeningTime : " << screeningTime << endl;

			fout.close();

			for (int n = 0; n < NumSelect; ++n) {
				resout << nameMethod(pFilter[n]->getMethod()) << '\t';
				resout << nameSize(pFilter[n]->getSize()) << '\t';
			}
			resout << "set" << set + 1 << '\t' << successRatio << '\t' << screeningTime << '\n';
			resout.flush();

			cout << "done" << endl;
		}

		for (int th = 0; th < NumThreads; ++th) {
			delete pFilter[th];
		}
	}

	resout.close();
	
	return 0;
}


double NCC(const Mat img1, const Mat img2){
	Mat result;
	matchTemplate(img1, img2, result, CV_TM_CCORR_NORMED);
	return result.at<float>(0, 0);
}

void MakeFilterObject(Filter** pFilter, filter filter){
	switch (filter.method){
	case RGBval:
		*pFilter = new FilterRGBval(filter);
		break;
	case GRAYval:
		*pFilter = new FilterGRAYval(filter);
		break;
	case RGBvalNCC:
		*pFilter = new FilterRGBvalNCC(filter);
		break;
	case GRAYvalNCC:
		*pFilter = new FilterGRAYvalNCC(filter);
		break;
	case RGBhist:
		*pFilter = new FilterRGBhist(filter);
		break;
	case GRAYhist:
		*pFilter = new FilterGRAYhist(filter);
		break;
	case LBPhist:
		*pFilter = new FilterLBPhist(filter);
		break;
	case HOG:
		*pFilter = new FilterHOG(filter);
		break;
	case CCV:
		*pFilter = new FilterCCV(filter);
		break;
	case Gabor:
		*pFilter = new FilterGabor(filter);
		break;
	case SURF:
		*pFilter = new FilterSURF(filter);
		break;
	case Method::AKAZE:
		*pFilter = new FilterAKAZE(filter);
		break;
	case SURFmatch:
		*pFilter = new FilterSURFmatch(filter);
		break;
	case AKAZEmatch:
		*pFilter = new FilterAKAZEmatch(filter);
		break;
	case LSDnumber:
		*pFilter = new FilterLSDnumber(filter);
		break;
	case LSDlength:
		*pFilter = new FilterLSDlength(filter);
		break;
	case LSDregion:
		*pFilter = new FilterLSDregion(filter);
		break;
	case LSDparallel:
		*pFilter = new FilterLSDparallel(filter);
		break;
	case LSDdistance:
		*pFilter = new FilterLSDdistance(filter);
		break;
	case LSDangle:
		*pFilter = new FilterLSDangle(filter);
		break;
	case LSDsolo:
		*pFilter = new FilterLSDsolo(filter);
		break;
	case LSDcombi:
		*pFilter = new FilterLSDcombi(filter);
		break;
	case RF:
		*pFilter = new FilterRF(filter);
		break;
	case RForiginal:
		*pFilter = new FilterRForiginal(filter);
		break;
	case RFwithout:
		*pFilter = new FilterRFwithout(filter);
		break;
	}
}

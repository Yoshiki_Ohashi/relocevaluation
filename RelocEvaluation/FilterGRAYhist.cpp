#include "FilterGRAYhist.h"

using namespace cv;


FilterGRAYhist::FilterGRAYhist() : Filter(){

}

FilterGRAYhist::FilterGRAYhist(int thread) : Filter(thread){

}

FilterGRAYhist::FilterGRAYhist(Method method) : Filter(method){

}

FilterGRAYhist::FilterGRAYhist(int thread, Method method) : Filter(thread, method){

}

FilterGRAYhist::FilterGRAYhist(Size size) : Filter(size){

}

FilterGRAYhist::FilterGRAYhist(int thread, Size size) : Filter(thread, size){

}

FilterGRAYhist::FilterGRAYhist(Method method, Size size) : Filter(method, size){

}

FilterGRAYhist::FilterGRAYhist(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterGRAYhist::FilterGRAYhist(filter filter) : Filter(filter){

}

FilterGRAYhist::FilterGRAYhist(int thread, filter filter) : Filter(thread, filter){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO) : Filter(imgIO){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterGRAYhist::FilterGRAYhist(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}

FilterGRAYhist::~FilterGRAYhist()
{
}

void FilterGRAYhist::calcFeature(){
	int histSize[] = { 32 };
	float range[] = { 0, 256 };
	const float* ranges[] = { range };
	int channels[] = { 0 };
	calcHist(&ImgIO->getResizedGRAY(thread), 1, channels, Mat(), descriptor, 1, histSize, ranges, true, false);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterGRAYhist)
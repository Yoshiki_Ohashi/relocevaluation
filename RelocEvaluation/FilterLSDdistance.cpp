#include "FilterLSDdistance.h"

using namespace cv;


FilterLSDdistance::FilterLSDdistance() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::FilterLSDdistance(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDdistance::~FilterLSDdistance()
{
}

void FilterLSDdistance::calcFeature() {
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = cv::Mat::zeros(cv::Size(4, 1), CV_32S);
	int* des = descriptor.ptr<int>(0);
	double distance[4];
	double mina, minb;
	for (int i = 0; i < (int)lines.size() - 1; ++i) {
		for (int j = i + 1; j < (int)lines.size(); ++j) {
			distance[0] = (lines[i][0] - lines[j][0])*(lines[i][0] - lines[j][0]) + (lines[i][1] - lines[j][1])*(lines[i][1] - lines[j][1]);
			distance[1] = (lines[i][0] - lines[j][0])*(lines[i][2] - lines[j][2]) + (lines[i][1] - lines[j][1])*(lines[i][3] - lines[j][3]);
			distance[2] = (lines[i][2] - lines[j][2])*(lines[i][0] - lines[j][0]) + (lines[i][3] - lines[j][3])*(lines[i][1] - lines[j][1]);
			distance[3] = (lines[i][2] - lines[j][2])*(lines[i][2] - lines[j][2]) + (lines[i][3] - lines[j][3])*(lines[i][3] - lines[j][3]);
			mina = min(distance[0], distance[1]);
			minb = min(distance[2], distance[3]);
			mina = min(mina, minb);
			if (mina < th4) {
				if (mina < th2) {
					if (mina < th1) des[0] += 1;
					else des[1] += 1;
				}
				else {
					if (mina < th3) des[2] += 1;
					else des[3] += 1;
				}
			}
		}
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDdistance)
#include "ImageIO.h"
using namespace cv;

ImageIO::ImageIO()
{
	ID_RGB = -1;
	ID_GRAY = -1;
	ID_DEPTH = -1;
}

ImageIO::ImageIO(string path)
{
	ID_RGB = -1;
	ID_GRAY = -1;
	ID_DEPTH = -1;
	setDBpath(path);
}

ImageIO::~ImageIO()
{
}

void ImageIO::read(int ID){
	read(ID, RGB);
	read(ID, GRAY);
	read(ID, DEPTH);
}

void ImageIO::read(int ID, imgType type){
	switch (type){
	case RGB:
		if (ID_RGB != ID){
			ID_RGB = ID;
			char filename[32];
			nameRGB(filename, ID);
			string path = DBpath + filename;
			rgb = imread(path, 1);
		}
		break;
	case GRAY:
		if (ID_GRAY != ID){
			ID_GRAY = ID;
			char filename[32];
			nameRGB(filename, ID);
			string path = DBpath + filename;
			gray = imread(path, 0);
		}
		break;
	case DEPTH:
		if (ID_DEPTH != ID){
			ID_DEPTH = ID;
			char filename[32];
			nameDEPTH(filename, ID);
			string path = DBpath + filename;
			depth = imread(path, -1);
		}
		break;
	}
}

void ImageIO::read(int ID, Size size, int thread){
	read(ID, RGB, size, thread);
	read(ID, GRAY, size, thread);
	read(ID, DEPTH, size, thread);
}

void ImageIO::read(int ID, imgType type, Size size, int thread){
	boost::mutex::scoped_lock lock(sync);
	switch (type){
	case RGB:
		read(ID, RGB);
		resize(rgb, resizedRGB[thread], size);
		break;
	case GRAY:
		read(ID, GRAY);
		resize(gray, resizedGRAY[thread], size);
		break;
	case DEPTH:
		read(ID, DEPTH);
		resize(depth, resizedDEPTH[thread], size);
		break;
	}
}

void ImageIO::read(Size size, int thread){
	read(RGB, size, thread);
	read(GRAY, size, thread);
	read(DEPTH, size, thread);
}

void ImageIO::read(imgType type, Size size, int thread){
	boost::mutex::scoped_lock lock(sync);
	switch (type){
	case RGB:
		resize(rgb, resizedRGB[thread], size);
		break;
	case GRAY:
		resize(gray, resizedGRAY[thread], size);
		break;
	case DEPTH:
		resize(depth, resizedDEPTH[thread], size);
		break;
	}
}

void ImageIO::write(){

}

void ImageIO::write(imgType type){

}

void ImageIO::write(imgType type, int thread){

}
#include "FilterLSDcombi.h"

using namespace cv;


FilterLSDcombi::FilterLSDcombi() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::FilterLSDcombi(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDcombi::~FilterLSDcombi()
{
}

void FilterLSDcombi::calcFeature() {
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = cv::Mat::zeros(cv::Size(16, 1), CV_32S);
	int* des = descriptor.ptr<int>(0);
	double distance[4];
	double mina, minb;
	Vec2f veca, vecb;
	float norma, normb;
	double angle;
	int bin;
	for (int i = 0; i < (int)lines.size() - 1; ++i) {
		veca[0] = lines[i][0] - lines[i][2];
		veca[1] = lines[i][1] - lines[i][3];
		norma = veca[0] * veca[0] + veca[1] * veca[1];

		for (int j = i + 1; j < (int)lines.size(); ++j) {
			distance[0] = (lines[i][0] - lines[j][0])*(lines[i][0] - lines[j][0]) + (lines[i][1] - lines[j][1])*(lines[i][1] - lines[j][1]);
			distance[1] = (lines[i][0] - lines[j][0])*(lines[i][2] - lines[j][2]) + (lines[i][1] - lines[j][1])*(lines[i][3] - lines[j][3]);
			distance[2] = (lines[i][2] - lines[j][2])*(lines[i][0] - lines[j][0]) + (lines[i][3] - lines[j][3])*(lines[i][1] - lines[j][1]);
			distance[3] = (lines[i][2] - lines[j][2])*(lines[i][2] - lines[j][2]) + (lines[i][3] - lines[j][3])*(lines[i][3] - lines[j][3]);
			mina = min(distance[0], distance[1]);
			minb = min(distance[2], distance[3]);
			mina = min(mina, minb);

			vecb[0] = lines[j][0] - lines[j][2];
			vecb[1] = lines[j][1] - lines[j][3];
			normb = vecb[0] * vecb[0] + vecb[1] * vecb[1];
			angle = (veca[0] * vecb[0] + veca[1] * vecb[1])*(veca[0] * vecb[0] + veca[1] * vecb[1]) / norma / normb;

			if (angle > ang_th2) {
				if (angle > ang_th1) bin = 0;
				else bin = 4;
			}
			else {
				if (angle > ang_th3) bin = 8;
				else bin = 12;
			}

			if (mina < dis_th4) {
				if (mina < dis_th2) {
					if (mina < dis_th1) des[bin] += 1;
					else des[bin + 1] += 1;
				}
				else {
					if (mina < dis_th3) des[bin + 2] += 1;
					else des[bin + 3] += 1;
				}
			}
		}
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDcombi)
#include "Relocalizer.h"
#include <boost/bind.hpp>
#include <boost/thread.hpp>

Relocalizer::Relocalizer()
{
}


Relocalizer::~Relocalizer()
{
}

void Relocalizer::initialize(){
	set_DBFolderPath("C:\\Users\Ohashi\\Desktop\\fugen\\area1\\20161207143131");
	set_QueryFolderPath("C:\\Users\Ohashi\\Desktop\\fugen\\area1\\20161207143131");

	screen RF;
	RF.resolution = Size(1920, 1080);
	RF.pre_process = none;
	RF.method = Randomized_Fern;
	RF.similarity = euclidean;
	RF.DB_Path = "DB_RF_1920_1080.txt";
	add_screen(RF);
}

void Relocalizer::makeDB(){
	for (int i = 0; i < screens.size(); i++){
		switch(screens[i].method){
		case Randomized_Fern:
			makeDB_RF(screens[i]);
		}
	}
}

void Relocalizer::run(){
	boost::thread **threads;
	threads = new boost::thread*[screens.size()];
	for (int i = 0; i < screens.size(); i++){
		threads[i] = new boost::thread(screening, screens[i]);
	}
	for (int i = 0; i < screens.size(); i++){
		threads[i]->join();
	}
	for (int i = 0; i < screens.size(); i++){
		delete threads[i];
	}
	delete[] threads;
}

void Relocalizer::screening(screen screen){
	switch (screen.method){
	case Randomized_Fern:
		screening_RF(screen);
	}
}
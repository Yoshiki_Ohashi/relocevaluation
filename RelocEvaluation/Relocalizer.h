#pragma once

#include <vector>
#include <string>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

enum pre_process
{
	none = 0,
	binarization = 1,
	gaussian = 2,
	lsd = 3,
	laplacian = 4,
	hough = 5,
};

enum method
{
	Randomized_Fern = 0,
};

enum similarity
{
	manhattan = 0,
	euclidean = 1,
	correlation = 2,
};

typedef struct{
	Size resolution;
	pre_process pre_process;
	method method;
	similarity similarity;
	vector<double> parameters;
	string DB_Path;
}screen;

class Relocalizer
{
public:
	Relocalizer();
	~Relocalizer();
	
	void initialize();
	void makeDB();
	void run();

private:
	void add_screen(screen screen){
		screens.push_back(screen);
	}

	void set_DBFolderPath(string path){
		DB_Folder_Path = path;
	}

	void set_QueryFolderPath(string path){
		Query_Folder_Path = path;
	}

	void makeDB_RF(screen);

	void screening(screen);
	void screening_RF(screen);

	vector<screen> screens;
	string DB_Folder_Path;
	string Query_Folder_Path;
};


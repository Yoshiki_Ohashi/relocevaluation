#pragma once
#include "Filter.h"

class FilterRF :
	public Filter
{
public:
	FilterRF();
	FilterRF(int);
	FilterRF(Method);
	FilterRF(int, Method);
	FilterRF(cv::Size);
	FilterRF(int, cv::Size);
	FilterRF(Method, cv::Size);
	FilterRF(int, Method, cv::Size);
	FilterRF(filter);
	FilterRF(int, filter);
	FilterRF(ImageIO* imgIO);
	FilterRF(ImageIO* imgIO, int);
	FilterRF(ImageIO* imgIO, Method);
	FilterRF(ImageIO* imgIO, int, Method);
	FilterRF(ImageIO* imgIO, cv::Size);
	FilterRF(ImageIO* imgIO, int, cv::Size);
	FilterRF(ImageIO* imgIO, Method, cv::Size);
	FilterRF(ImageIO* imgIO, int, Method, cv::Size);
	FilterRF(ImageIO* imgIO, filter);
	FilterRF(ImageIO* imgIO, int, filter);
	~FilterRF();
	bool checkSpeed(int);
	void makeDB();
	int getDBsize() {
		return filledDB;
	}
	void init();

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version) {
		ar & boost::serialization::base_object<Filter>(*this);
		for (int i = 0; i < numFerns; ++i) {
			for (int j = 0; j < 4; ++j) {
				ar & positions[4 * i + j];
				ar & thresholds[4 * i + j];
			}
			for (int j = 0; j < 16; ++j) {
				ar & tables[16 * i + j];
			}
		}
	}

	const cv::Mat kernel = cv::getGaussianKernel(15, 2.5, CV_64F); //sigma=2.5, ksizeは2*3*sigmaに近い奇数（正規分布では全体の99.73%が-3sigmaから+3sigmaに含まれる）
	cv::Mat blurred;

	uchar* descriptor = new uchar[numFerns];
	int* positions = new int[4 * numFerns];
	uchar* thresholds = new uchar[4 * numFerns];
	float** pPositions = new float*[4 * numFerns];
	vector<int>* tables = new vector<int>[16 * numFerns];
	int coocurrence[DBsize];

	void randomizePosAndThresh();
	void allocMat();
	void getPointers();

	void makeDB(int);
	void clearDB() {
		for (int i = 0; i < 16 * numFerns; ++i) {
			tables[i].clear();
		}
	}

	uchar** descriptors = new uchar*[DBsize];
	int filledDB = 0;
	bool hasToCount;

	void extractFeature(int ID) {
		extractFeatureFromRGB(ID);
	}
	void extractFeature() {
		extractFeatureFromRGB();
	}
	void calcFeature();
	void addIDtoTable(int);
	void addFeatureToDB();
	void countCooccurrence();
	double compare(int ID) {
		return numFerns - coocurrence[ID];
	}
	void generateUniqueRandomArray(vector<int>&, const int, const int, const int);
};


BOOST_CLASS_EXPORT_KEY(FilterRF)
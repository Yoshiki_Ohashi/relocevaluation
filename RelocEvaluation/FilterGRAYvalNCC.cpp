#include "FilterGRAYvalNCC.h"

using namespace cv;

FilterGRAYvalNCC::FilterGRAYvalNCC() : Filter(){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(int thread) : Filter(thread){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(Method method) : Filter(method){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(int thread, Method method) : Filter(thread, method){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(Size size) : Filter(size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(int thread, Size size) : Filter(thread, size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(Method method, Size size) : Filter(method, size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(filter filter) : Filter(filter){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(int thread, filter filter) : Filter(thread, filter){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO) : Filter(imgIO){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterGRAYvalNCC::FilterGRAYvalNCC(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}


FilterGRAYvalNCC::~FilterGRAYvalNCC()
{
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterGRAYvalNCC)
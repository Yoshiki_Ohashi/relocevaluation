#pragma once
#include "Filter.h"
class FilterHOG :
	public Filter
{
public:
	FilterHOG();
	FilterHOG(int);
	FilterHOG(Method);
	FilterHOG(int, Method);
	FilterHOG(cv::Size);
	FilterHOG(int, cv::Size);
	FilterHOG(Method, cv::Size);
	FilterHOG(int, Method, cv::Size);
	FilterHOG(filter);
	FilterHOG(int, filter);
	FilterHOG(ImageIO* imgIO);
	FilterHOG(ImageIO* imgIO, int);
	FilterHOG(ImageIO* imgIO, Method);
	FilterHOG(ImageIO* imgIO, int, Method);
	FilterHOG(ImageIO* imgIO, cv::Size);
	FilterHOG(ImageIO* imgIO, int, cv::Size);
	FilterHOG(ImageIO* imgIO, Method, cv::Size);
	FilterHOG(ImageIO* imgIO, int, Method, cv::Size);
	FilterHOG(ImageIO* imgIO, filter);
	FilterHOG(ImageIO* imgIO, int, filter);
	~FilterHOG();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterHOG)

#include "FilterLSDparallel.h"

using namespace cv;


FilterLSDparallel::FilterLSDparallel() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::FilterLSDparallel(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDparallel::~FilterLSDparallel()
{
}

void FilterLSDparallel::calcFeature(){
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = cv::Mat::zeros(cv::Size(2, 1), CV_32S);
	int* des = descriptor.ptr<int>(0);
	double angle;
	for (int n = 0; n < (int)lines.size(); ++n){
		angle = abs((lines[n][1] - lines[n][3]) / (lines[n][0] - lines[n][2]));
		if (angle > vertical_th) des[0] += 1;
		if (angle < horizontal_th) des[1] += 1;
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDparallel)
#include "FilterRGBval.h"

using namespace cv;


FilterRGBval::FilterRGBval() : Filter(){

}

FilterRGBval::FilterRGBval(int thread) : Filter(thread){

}

FilterRGBval::FilterRGBval(Method method) : Filter(method){

}

FilterRGBval::FilterRGBval(int thread, Method method) : Filter(thread, method){

}

FilterRGBval::FilterRGBval(Size size) : Filter(size){

}

FilterRGBval::FilterRGBval(int thread, Size size) : Filter(thread, size){

}

FilterRGBval::FilterRGBval(Method method, Size size) : Filter(method, size){

}

FilterRGBval::FilterRGBval(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterRGBval::FilterRGBval(filter filter) : Filter(filter){

}

FilterRGBval::FilterRGBval(int thread, filter filter) : Filter(thread, filter){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO) : Filter(imgIO){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterRGBval::FilterRGBval(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}


FilterRGBval::~FilterRGBval()
{
}

void FilterRGBval::calcFeature(){
	Mat desc[3];
	split(ImgIO->getResizedRGB(thread), desc);
	vconcat(desc, 3, descriptor);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterRGBval)
#pragma once
#include "Filter.h"
#include <vector>
class FilterRGBval :
	public Filter
{
public:
	FilterRGBval();
	FilterRGBval(int);
	FilterRGBval(Method);
	FilterRGBval(int, Method);
	FilterRGBval(cv::Size);
	FilterRGBval(int, cv::Size);
	FilterRGBval(Method, cv::Size);
	FilterRGBval(int, Method, cv::Size);
	FilterRGBval(filter);
	FilterRGBval(int, filter);
	FilterRGBval(ImageIO* imgIO);
	FilterRGBval(ImageIO* imgIO, int);
	FilterRGBval(ImageIO* imgIO, Method);
	FilterRGBval(ImageIO* imgIO, int, Method);
	FilterRGBval(ImageIO* imgIO, cv::Size);
	FilterRGBval(ImageIO* imgIO, int, cv::Size);
	FilterRGBval(ImageIO* imgIO, Method, cv::Size);
	FilterRGBval(ImageIO* imgIO, int, Method, cv::Size);
	FilterRGBval(ImageIO* imgIO, filter);
	FilterRGBval(ImageIO* imgIO, int, filter);
	~FilterRGBval();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	void clearDB(){
		descriptors.clear();
	}
	vector<cv::Mat> descriptors;
	void extractFeature(int ID){
		extractFeatureFromRGB(ID);
	}
	void extractFeature(){
		extractFeatureFromRGB();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterRGBval)
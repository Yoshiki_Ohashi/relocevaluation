#pragma once
#include "Filter.h"
class FilterGRAYval :
	public Filter
{
public:
	FilterGRAYval();
	FilterGRAYval(int);
	FilterGRAYval(Method);
	FilterGRAYval(int, Method);
	FilterGRAYval(cv::Size);
	FilterGRAYval(int, cv::Size);
	FilterGRAYval(Method, cv::Size);
	FilterGRAYval(int, Method, cv::Size);
	FilterGRAYval(filter);
	FilterGRAYval(int, filter);
	FilterGRAYval(ImageIO* imgIO);
	FilterGRAYval(ImageIO* imgIO, int);
	FilterGRAYval(ImageIO* imgIO, Method);
	FilterGRAYval(ImageIO* imgIO, int, Method);
	FilterGRAYval(ImageIO* imgIO, cv::Size);
	FilterGRAYval(ImageIO* imgIO, int, cv::Size);
	FilterGRAYval(ImageIO* imgIO, Method, cv::Size);
	FilterGRAYval(ImageIO* imgIO, int, Method, cv::Size);
	FilterGRAYval(ImageIO* imgIO, filter);
	FilterGRAYval(ImageIO* imgIO, int, filter);
	~FilterGRAYval();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature(){
		descriptor = ImgIO->getResizedGRAY(thread);
	}
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterGRAYval)

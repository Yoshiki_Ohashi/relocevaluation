#include "Filter.h"
using namespace cv;

boost::mutex Filter::sync[NumThreads - 1];

Filter::Filter()
{
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
}

Filter::Filter(int thread)
{
	SpeedIsChecked = false;
	setThreadNum(thread);
}

Filter::Filter(Method method){
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setMethod(method);
}

Filter::Filter(int thread, Method method){
	SpeedIsChecked = false;
	setThreadNum(thread);
	setMethod(method);
}

Filter::Filter(Size size){
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setSize(size);
}

Filter::Filter(int thread, Size size){
	SpeedIsChecked = false;
	setThreadNum(thread);
	setSize(size);
}

Filter::Filter(Method method, Size size){
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setMethod(method);
	setSize(size);
}

Filter::Filter(int thread, Method method, Size size){
	SpeedIsChecked = false;
	setThreadNum(thread);
	setMethod(method);
	setSize(size);
}

Filter::Filter(filter filter){
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setFilter(filter);
}

Filter::Filter(int thread, filter filter){
	SpeedIsChecked = false;
	setThreadNum(thread);
	setFilter(filter);
}

Filter::Filter(ImageIO* imgIO)
{
	ImgIO = imgIO;
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
}

Filter::Filter(ImageIO* imgIO, int thread)
{
	ImgIO = imgIO;
	SpeedIsChecked = false;
	setThreadNum(thread);
}

Filter::Filter(ImageIO* imgIO, Method method){
	ImgIO = imgIO;
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setMethod(method);
}

Filter::Filter(ImageIO* imgIO, int thread, Method method){
	ImgIO = imgIO;
	SpeedIsChecked = false;
	setThreadNum(thread);
	setMethod(method);
}

Filter::Filter(ImageIO* imgIO, Size size){
	ImgIO = imgIO;
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setSize(size);
}

Filter::Filter(ImageIO* imgIO, int thread, Size size){
	ImgIO = imgIO;
	SpeedIsChecked = false;
	setThreadNum(thread);
	setSize(size);
}

Filter::Filter(ImageIO* imgIO, Method method, Size size){
	ImgIO = imgIO;
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setMethod(method);
	setSize(size);
}

Filter::Filter(ImageIO* imgIO, int thread, Method method, Size size){
	ImgIO = imgIO;
	SpeedIsChecked = false;
	setThreadNum(thread);
	setMethod(method);
	setSize(size);
}

Filter::Filter(ImageIO* imgIO, filter filter){
	ImgIO = imgIO;
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false;
	SpeedIsChecked = false;
	setFilter(filter);
}

Filter::Filter(ImageIO* imgIO, int thread, filter filter){
	ImgIO = imgIO;
	SpeedIsChecked = false;
	setThreadNum(thread);
	setFilter(filter);
}

Filter::~Filter(){

}

void Filter::setThreadNum(int th){
	isFirstFilter = false;
	isLastFilter = false;
	isFinished = false; 
	thread = th;
	if (th == 0) isFirstFilter = true;
	if (th == NumThreads - 1) isLastFilter = true;
}

bool Filter::checkSpeed(int repeat){
	if (!SpeedIsChecked){
		random_device rnd;
		uniform_int_distribution<> random(0, DBsize - 1);
		clearDB();
		makeDB(repeat);
		timePrepare = 0;
		timeCompare = 0;
		for (int i = 0; i < repeat; i++){
			ImgIO->read(random(rnd));
			screening(repeat, &timePrepare, &timeCompare);
		}
		clearDB();
		timePrepare = timePrepare * 1000 / (double)repeat;
		timeCompare = timeCompare * 1000 / (double)repeat / (double)repeat;
		SpeedIsChecked = true;
	}
	double time = timePrepare + timeCompare*candidateSize;
	if (time < requiredTime) return true;
	else return false;
}

void Filter::makeDB(){
	for (int ID = 0; ID < DBsize; ID++){
		extractFeature(ID);
		addFeatureToDB();
	}
}

void Filter::makeDB(int repeat){
	random_device rnd; 
	uniform_int_distribution<> random(0, DBsize - 1);
	for (int i = 0; i < repeat; i++){
		extractFeature(random(rnd));
		addFeatureToDB();
	}
}

void Filter::screening(int repeat, double* timePrepare, double* timeCompare){ //for checkSpeed
	vector<int> nextQueue;
	thresh = 10;
	LARGE_INTEGER freq, startPrepare, endPrepare, startCompare, endCompare;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&startPrepare);
	extractFeature();
	QueryPerformanceCounter(&endPrepare);
	QueryPerformanceCounter(&startCompare);
	for (int ID = 0; ID < repeat; ID++){
		double distance = compare(ID);
		if (distance <= thresh) nextQueue.push_back(ID);
	}
	QueryPerformanceCounter(&endCompare);
	*timePrepare += (double)(endPrepare.QuadPart - startPrepare.QuadPart) / freq.QuadPart;
	*timeCompare += (double)(endCompare.QuadPart - startCompare.QuadPart) / freq.QuadPart;
}

double Filter::screening(vector<double>* correct, vector<double>* wrong, int minID, int maxID){
	int QueryID = ImgIO->getID();
	LARGE_INTEGER freq, start, end;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&start);
	extractFeature();
	for (int ID = 0; ID < minID; ID++){
		wrong->push_back(compare(ID));
	}
	for (int ID = minID; ID < QueryID; ID++){
		correct->push_back(compare(ID));
	}
	for (int ID = QueryID + 1; ID <= maxID; ID++){
		correct->push_back(compare(ID));
	}
	for (int ID = maxID + 1; ID < DBsize; ID++){
		wrong->push_back(compare(ID));
	}
	QueryPerformanceCounter(&end);
	return (double)(end.QuadPart - start.QuadPart) / freq.QuadPart;
}

void Filter::screeningFL(vector<int>* thisQueue, vector<int>* nextQueue) {
	extractFeature();

	int count = 0;
	double distMIN = DBL_MAX;
	int IDofdistMIN;

	int imax = thisQueue->size();
	for (int i = 0; i < imax; ++i) {
		double distance = compare((*thisQueue)[i]);
		if (distance < distMIN) {
			distMIN = distance;
			IDofdistMIN = (*thisQueue)[i];
		}
		++count;
		if (count == CuttingRatio) {
			nextQueue->push_back(IDofdistMIN);
			count = 0;
			distMIN = DBL_MAX;
		}
	}
	if (count != 0) nextQueue->push_back(IDofdistMIN);
	isFinished = true;
}

void Filter::screeningF(vector<int>* thisQueue, vector<int>* nextQueue) {
	extractFeature();

	int count = 0;
	double distMIN = DBL_MAX;
	int IDofdistMIN;

	int imax = thisQueue->size();
	for (int i = 0; i < imax; ++i) {
		double distance = compare((*thisQueue)[i]);
		if (distance < distMIN) {
			distMIN = distance;
			IDofdistMIN = (*thisQueue)[i];
		}
		++count;
		if (count == CuttingRatio) {
			pLockNext = new boost::mutex::scoped_lock(sync[0]);
			nextQueue->push_back(IDofdistMIN);
			delete pLockNext;
			count = 0;
			distMIN = DBL_MAX;
		}
	}
	if (count != 0) {
		pLockNext = new boost::mutex::scoped_lock(sync[0]);
		nextQueue->push_back(IDofdistMIN);
		delete pLockNext;
	}
	isFinished = true;
}

void Filter::screeningL(vector<int>* thisQueue, vector<int>* nextQueue, volatile bool* prevIsFinished) {
	extractFeature();

	int count = 0;
	double distMIN = DBL_MAX;
	int IDofdistMIN;

	int finished = 0;
	double distance;
	while (true) {
		if (thisQueue->size() > finished) {
			pLockPrev = new boost::mutex::scoped_lock(sync[thread - 1]);
			if (compareWithIDcheck((*thisQueue)[finished], &distance)) {
				if (distance < distMIN) {
					distMIN = distance;
					IDofdistMIN = (*thisQueue)[finished];
				}
				++count;
				if (count == CuttingRatio) {
					nextQueue->push_back(IDofdistMIN);
					count = 0;
					distMIN = DBL_MAX;
				}
				++finished;
			}
			delete pLockPrev;
		}
		else if (*prevIsFinished) break;
	}
	if (count != 0) nextQueue->push_back(IDofdistMIN);
	isFinished = true;
}

void Filter::screeningN(vector<int>* thisQueue, vector<int>* nextQueue, volatile bool* prevIsFinished) {
	extractFeature();

	int count = 0;
	double distMIN = DBL_MAX;
	int IDofdistMIN;

	int finished = 0;
	double distance;
	while (true) {
		if (thisQueue->size() > finished) {
			pLockPrev = new boost::mutex::scoped_lock(sync[thread - 1]);
			if (compareWithIDcheck((*thisQueue)[finished], &distance)) {
				if (distance < distMIN) {
					distMIN = distance;
					IDofdistMIN = (*thisQueue)[finished];
				}
				++count;
				if (count == CuttingRatio) {
					pLockNext = new boost::mutex::scoped_lock(sync[thread]);
					nextQueue->push_back(IDofdistMIN);
					delete pLockNext;
					count = 0;
					distMIN = DBL_MAX;
				}
				++finished;
			}
			delete pLockPrev;
		}
		else if (*prevIsFinished) break;
	}
	if (count != 0) {
		pLockNext = new boost::mutex::scoped_lock(sync[thread]);
		nextQueue->push_back(IDofdistMIN);
		delete pLockNext;
	}
	isFinished = true;
}

bool Filter::compareWithIDcheck(int ID, double* distance) {
	if (ID < 0) return false;
	if (ID >= DBsize) return false;
	*distance = compare(ID);
	return true;
}

string nameMethod(Method method){
	switch (method){
	case RGBval:
		return "RGBvalue";
		break;
	case GRAYval:
		return "GRAYvalue";
		break;
	case RGBvalNCC:
		return "RGBvalueNCC";
		break;
	case GRAYvalNCC:
		return "GRAYvalueNCC";
		break;
	case RGBhist:
		return "RGBhistgram";
		break;
	case GRAYhist:
		return "GRAYhistgram";
		break;
	case LBPhist:
		return "LBPhistgram";
		break;
	case HOG:
		return "HOG";
		break;
	case CCV:
		return "CCV";
		break;
	case Gabor:
		return "Gabor";
		break;
	case SURF:
		return "SURF";
		break;
	case Method::AKAZE:
		return "AKAZE";
		break;
	case SURFmatch:
		return "SURFmatch";
		break;
	case AKAZEmatch:
		return "AKAZEmatch";
		break;
	case LSDnumber:
		return "LSDnumber";
		break;
	case LSDlength:
		return "LSDlength";
		break;
	case LSDregion:
		return "LSDregion";
		break;
	case LSDparallel:
		return "LSDparallel";
		break;
	case LSDdistance:
		return "LSDdistance";
		break;
	case LSDangle:
		return "LSDangle";
		break;
	case LSDsolo:
		return "LSDsolo";
		break;
	case LSDcombi:
		return "LSDcombi";
		break;
	case RF:
		return "RandomizedFern";
		break;
	case RForiginal:
		return "RForiginal";
		break;
	case RFwithout:
		return "RFwithoutTable";
		break;
	default:
		return "none";
		break;
	}
}

string nameSize(cv::Size size){
	char name[32];
	sprintf_s(name, "%dx%d", size.width, size.height);
	return string(name);
}

void copy(filter& filter, Filter* other){
	filter.method = other->getMethod();
	filter.size = other->getSize();
	filter.thresh = other->getThresh();
	filter.SpeedIsChecked = other->SpeedIsChecked;
	filter.timePrepare = other->timePrepare;
	filter.timeCompare = other->timeCompare;
}

bool compareKeypoints(const cv::KeyPoint& left, const cv::KeyPoint& right) {
	if (left.response == right.response) {
		return left.size > right.size;
	}
	else return left.response > right.response;
}

bool compareResult(const result& left, const result& right) {
	return left.distance < right.distance;
}

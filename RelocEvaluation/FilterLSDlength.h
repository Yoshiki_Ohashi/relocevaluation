#pragma once
#include "Filter.h"

class FilterLSDlength :
	public Filter
{
public:
	FilterLSDlength();
	FilterLSDlength(int);
	FilterLSDlength(Method);
	FilterLSDlength(int, Method);
	FilterLSDlength(cv::Size);
	FilterLSDlength(int, cv::Size);
	FilterLSDlength(Method, cv::Size);
	FilterLSDlength(int, Method, cv::Size);
	FilterLSDlength(filter);
	FilterLSDlength(int, filter);
	FilterLSDlength(ImageIO* imgIO);
	FilterLSDlength(ImageIO* imgIO, int);
	FilterLSDlength(ImageIO* imgIO, Method);
	FilterLSDlength(ImageIO* imgIO, int, Method);
	FilterLSDlength(ImageIO* imgIO, cv::Size);
	FilterLSDlength(ImageIO* imgIO, int, cv::Size);
	FilterLSDlength(ImageIO* imgIO, Method, cv::Size);
	FilterLSDlength(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDlength(ImageIO* imgIO, filter);
	FilterLSDlength(ImageIO* imgIO, int, filter);
	~FilterLSDlength();

	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	const double th1 = 25; //5^2
	const double th2 = 100; //10^2
	const double th3 = 400; //20^2

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDlength)
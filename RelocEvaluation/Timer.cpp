/*! @file
    @brief Class which measures time in various methods.

	@author Hirotake Ishii (hirotake@ieee.org)
    @date   2008-04-16
    @version 3.0

	Copyright(C) 2005-2008 Hirotake Ishii All Rights Reserved.
*/

//#include "stdafx.h"
#include "Timer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//static LARGE_INTEGER i64Frequency,i64LastTime,i64NowTime,i64Diff;

CTimer::CTimer()
{
	QueryPerformanceFrequency( (LARGE_INTEGER*)&i64Frequency );
//	timeBeginPeriod(1);
}

CTimer::~CTimer()
{
//	timeEndPeriod(1);
}

//////////////////////////////////////////////////////////////////////
//
// Using CPU Frequency
//
//////////////////////////////////////////////////////////////////////

void CTimer::FreqClear()
{
	QueryPerformanceCounter( (LARGE_INTEGER*)&i64LastTime );
}

double CTimer::FreqGetTickInMilliSecond()
{
	QueryPerformanceCounter( (LARGE_INTEGER*)&i64NowTime );
	i64Diff.QuadPart = i64NowTime.QuadPart - i64LastTime.QuadPart;
	i64LastTime = i64NowTime;
    return (i64Diff.QuadPart*1.0 / i64Frequency.QuadPart)*1000;
}

double CTimer::FreqGetPassInMilliSecond()
{
	QueryPerformanceCounter( (LARGE_INTEGER*)&i64NowTime );
	i64Diff.QuadPart = i64NowTime.QuadPart - i64LastTime.QuadPart;
    return (i64Diff.QuadPart*1.0 / i64Frequency.QuadPart)*1000;
}

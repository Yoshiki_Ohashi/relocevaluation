#pragma once
#include "Filter.h"

class FilterLSDdistance :
	public Filter
{
public:
	FilterLSDdistance();
	FilterLSDdistance(int);
	FilterLSDdistance(Method);
	FilterLSDdistance(int, Method);
	FilterLSDdistance(cv::Size);
	FilterLSDdistance(int, cv::Size);
	FilterLSDdistance(Method, cv::Size);
	FilterLSDdistance(int, Method, cv::Size);
	FilterLSDdistance(filter);
	FilterLSDdistance(int, filter);
	FilterLSDdistance(ImageIO* imgIO);
	FilterLSDdistance(ImageIO* imgIO, int);
	FilterLSDdistance(ImageIO* imgIO, Method);
	FilterLSDdistance(ImageIO* imgIO, int, Method);
	FilterLSDdistance(ImageIO* imgIO, cv::Size);
	FilterLSDdistance(ImageIO* imgIO, int, cv::Size);
	FilterLSDdistance(ImageIO* imgIO, Method, cv::Size);
	FilterLSDdistance(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDdistance(ImageIO* imgIO, filter);
	FilterLSDdistance(ImageIO* imgIO, int, filter);
	~FilterLSDdistance();

	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	const double th1 = 25; //5^2
	const double th2 = 100; //10^2
	const double th3 = 400; //20^2
	const double th4 = 1600; //40^2

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDdistance)
#pragma once
#include "Filter.h"
class FilterGRAYhist :
	public Filter
{
public:
	FilterGRAYhist();
	FilterGRAYhist(int);
	FilterGRAYhist(Method);
	FilterGRAYhist(int, Method);
	FilterGRAYhist(cv::Size);
	FilterGRAYhist(int, cv::Size);
	FilterGRAYhist(Method, cv::Size);
	FilterGRAYhist(int, Method, cv::Size);
	FilterGRAYhist(filter);
	FilterGRAYhist(int, filter);
	FilterGRAYhist(ImageIO* imgIO);
	FilterGRAYhist(ImageIO* imgIO, int);
	FilterGRAYhist(ImageIO* imgIO, Method);
	FilterGRAYhist(ImageIO* imgIO, int, Method);
	FilterGRAYhist(ImageIO* imgIO, cv::Size);
	FilterGRAYhist(ImageIO* imgIO, int, cv::Size);
	FilterGRAYhist(ImageIO* imgIO, Method, cv::Size);
	FilterGRAYhist(ImageIO* imgIO, int, Method, cv::Size);
	FilterGRAYhist(ImageIO* imgIO, filter);
	FilterGRAYhist(ImageIO* imgIO, int, filter);
	~FilterGRAYhist();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterGRAYhist)
#include "FilterAKAZEmatch.h"

using namespace cv;

FilterAKAZEmatch::FilterAKAZEmatch() : Filter() {
}

FilterAKAZEmatch::FilterAKAZEmatch(int thread) : Filter(thread) {
}

FilterAKAZEmatch::FilterAKAZEmatch(Method method) : Filter(method) {
}

FilterAKAZEmatch::FilterAKAZEmatch(int thread, Method method) : Filter(thread, method) {
}

FilterAKAZEmatch::FilterAKAZEmatch(Size size) : Filter(size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(int thread, Size size) : Filter(thread, size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(Method method, Size size) : Filter(method, size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(int thread, Method method, Size size) : Filter(thread, method, size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(filter filter) : Filter(filter) {
}

FilterAKAZEmatch::FilterAKAZEmatch(int thread, filter filter) : Filter(thread, filter) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO) : Filter(imgIO) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, int thread) : Filter(imgIO, thread) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, Method method) : Filter(imgIO, method) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, Size size) : Filter(imgIO, size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, filter filter) : Filter(imgIO, filter) {
}

FilterAKAZEmatch::FilterAKAZEmatch(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter) {
}

FilterAKAZEmatch::~FilterAKAZEmatch()
{
}

void FilterAKAZEmatch::calcFeature() {
	Mat src = ImgIO->getResizedGRAY(thread);
	detector->detect(src, keypoints);
	if (keypoints.size() > 200) {
		std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);
		keypoints.erase(keypoints.begin() + 200, keypoints.end());
	}
	detector->compute(src, keypoints, descriptor);
}

double FilterAKAZEmatch::compare(int ID) {
	vector<cv::DMatch> dmatch, dmatch1, dmatch2;

	matcher->match(descriptor, descriptors[ID], dmatch1);
	matcher->match(descriptors[ID], descriptor, dmatch2);

	for (size_t i = 0; i < dmatch1.size(); ++i)
	{
		cv::DMatch forward = dmatch1[i];
		cv::DMatch backward = dmatch2[forward.trainIdx];
		if (backward.trainIdx == forward.queryIdx)
			dmatch.push_back(forward);
	}

	return ((double)dmatch1.size() - (double)dmatch.size()) / (double)descriptor.rows;

	//double avgDistance = 0;
	//int numMatch = (int)dmatch.size();

	//if (numMatch == 0) {
	//	return DBL_MAX;
	//}

	//for (int i = 0; i < numMatch; ++i) {
	//	Point2f keypoint1 = keypoints[dmatch[i].queryIdx].pt;
	//	Point2f keypoint2 = vecKeypoints[ID][dmatch[i].trainIdx].pt;
	//	Point2f difference = keypoint1 - keypoint2;
	//	avgDistance += sqrt(difference.x*difference.x + difference.y*difference.y);
	//}

	//avgDistance /= numMatch;

	//return avgDistance;
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterAKAZEmatch)

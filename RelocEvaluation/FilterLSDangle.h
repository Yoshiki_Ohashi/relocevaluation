#pragma once
#include "Filter.h"

class FilterLSDangle :
	public Filter
{
public:
	FilterLSDangle();
	FilterLSDangle(int);
	FilterLSDangle(Method);
	FilterLSDangle(int, Method);
	FilterLSDangle(cv::Size);
	FilterLSDangle(int, cv::Size);
	FilterLSDangle(Method, cv::Size);
	FilterLSDangle(int, Method, cv::Size);
	FilterLSDangle(filter);
	FilterLSDangle(int, filter);
	FilterLSDangle(ImageIO* imgIO);
	FilterLSDangle(ImageIO* imgIO, int);
	FilterLSDangle(ImageIO* imgIO, Method);
	FilterLSDangle(ImageIO* imgIO, int, Method);
	FilterLSDangle(ImageIO* imgIO, cv::Size);
	FilterLSDangle(ImageIO* imgIO, int, cv::Size);
	FilterLSDangle(ImageIO* imgIO, Method, cv::Size);
	FilterLSDangle(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDangle(ImageIO* imgIO, filter);
	FilterLSDangle(ImageIO* imgIO, int, filter);
	~FilterLSDangle();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	const float th1 = cosf(CV_PI / 8.)*cosf(CV_PI / 8.);
	const float th2 = cosf(CV_PI / 4.)*cosf(CV_PI / 4.);
	const float th3 = cosf(CV_PI*3. / 8.)*cosf(CV_PI*3. / 8.);

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDangle)
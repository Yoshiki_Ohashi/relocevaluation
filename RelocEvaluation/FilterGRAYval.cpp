#include "FilterGRAYval.h"

using namespace cv;

FilterGRAYval::FilterGRAYval() : Filter(){

}

FilterGRAYval::FilterGRAYval(int thread) : Filter(thread){

}

FilterGRAYval::FilterGRAYval(Method method) : Filter(method){

}

FilterGRAYval::FilterGRAYval(int thread, Method method) : Filter(thread, method){

}

FilterGRAYval::FilterGRAYval(Size size) : Filter(size){

}

FilterGRAYval::FilterGRAYval(int thread, Size size) : Filter(thread, size){

}

FilterGRAYval::FilterGRAYval(Method method, Size size) : Filter(method, size){

}

FilterGRAYval::FilterGRAYval(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterGRAYval::FilterGRAYval(filter filter) : Filter(filter){

}

FilterGRAYval::FilterGRAYval(int thread, filter filter) : Filter(thread, filter){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO) : Filter(imgIO){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterGRAYval::FilterGRAYval(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}


FilterGRAYval::~FilterGRAYval()
{
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterGRAYval)
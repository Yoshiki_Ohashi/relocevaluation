#pragma once
#include "Filter.h"

class FilterSURFmatch :
	public Filter
{
public:
	FilterSURFmatch();
	FilterSURFmatch(int);
	FilterSURFmatch(Method);
	FilterSURFmatch(int, Method);
	FilterSURFmatch(cv::Size);
	FilterSURFmatch(int, cv::Size);
	FilterSURFmatch(Method, cv::Size);
	FilterSURFmatch(int, Method, cv::Size);
	FilterSURFmatch(filter);
	FilterSURFmatch(int, filter);
	FilterSURFmatch(ImageIO* imgIO);
	FilterSURFmatch(ImageIO* imgIO, int);
	FilterSURFmatch(ImageIO* imgIO, Method);
	FilterSURFmatch(ImageIO* imgIO, int, Method);
	FilterSURFmatch(ImageIO* imgIO, cv::Size);
	FilterSURFmatch(ImageIO* imgIO, int, cv::Size);
	FilterSURFmatch(ImageIO* imgIO, Method, cv::Size);
	FilterSURFmatch(ImageIO* imgIO, int, Method, cv::Size);
	FilterSURFmatch(ImageIO* imgIO, filter);
	FilterSURFmatch(ImageIO* imgIO, int, filter);
	~FilterSURFmatch();

	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
		ar & vecKeypoints;
	}

	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	vector<cv::KeyPoint> keypoints;
	vector<vector<cv::KeyPoint>> vecKeypoints;
	cv::Ptr<cv::FeatureDetector> detector = cv::xfeatures2d::SURF::create();
	cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create("BruteForce");

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
		vecKeypoints.push_back(keypoints);
	}
	double compare(int ID);
};

BOOST_CLASS_EXPORT_KEY(FilterSURFmatch)

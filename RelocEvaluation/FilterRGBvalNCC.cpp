#include "FilterRGBvalNCC.h"

using namespace cv;


FilterRGBvalNCC::FilterRGBvalNCC() : Filter(){

}

FilterRGBvalNCC::FilterRGBvalNCC(int thread) : Filter(thread){

}

FilterRGBvalNCC::FilterRGBvalNCC(Method method) : Filter(method){

}

FilterRGBvalNCC::FilterRGBvalNCC(int thread, Method method) : Filter(thread, method){

}

FilterRGBvalNCC::FilterRGBvalNCC(Size size) : Filter(size){

}

FilterRGBvalNCC::FilterRGBvalNCC(int thread, Size size) : Filter(thread, size){

}

FilterRGBvalNCC::FilterRGBvalNCC(Method method, Size size) : Filter(method, size){

}

FilterRGBvalNCC::FilterRGBvalNCC(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterRGBvalNCC::FilterRGBvalNCC(filter filter) : Filter(filter){

}

FilterRGBvalNCC::FilterRGBvalNCC(int thread, filter filter) : Filter(thread, filter){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO) : Filter(imgIO){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterRGBvalNCC::FilterRGBvalNCC(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}


FilterRGBvalNCC::~FilterRGBvalNCC()
{
}

void FilterRGBvalNCC::calcFeature(){
	Mat desc[3];
	split(ImgIO->getResizedRGB(thread), desc);
	vconcat(desc, 3, descriptor);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterRGBvalNCC)
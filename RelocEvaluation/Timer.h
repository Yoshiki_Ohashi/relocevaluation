/*! @file
    @brief Class which measures time in various ways. This class can be used for maesuring the performance of the application.

	@author Hirotake Ishii (hirotake@ieee.org)
    @date   2008-04-16
    @version 3.0

	Copyright(C) 2005-2008 Hirotake Ishii All Rights Reserved.
*/
#include <afxwin.h>


#if !defined(AFX_TIMER_H__9C0AECD0_E214_4EF0_AFF8_C2500677CACD__INCLUDED_)
#define AFX_TIMER_H__9C0AECD0_E214_4EF0_AFF8_C2500677CACD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <mmsystem.h>

class CTimer  
{
public:

	LARGE_INTEGER i64Frequency,i64LastTime,i64NowTime,i64Diff;

	CTimer();
	virtual ~CTimer();

	/** Clear time. Set the time zero. (CPU frequency version)*/
	void FreqClear();

	/** Get the time passed since FreqClear or FreqGetTickInMilliSecond is called in millisecond. The time is initialized.(CPU frequency version)
	* @retval The time passed since FreqClear or FreqGetTickInMilliSecond is called.
	*/
	double FreqGetTickInMilliSecond();

	/** Get the time passed since FreqClear or FreqGetTickInMilliSecond is called in millisecond. The time is not initialized.(CPU frequency version)
	* @retval The time passed since FreqClear or FreqGetTickInMilliSecond is called.
	*/
	double FreqGetPassInMilliSecond();
};

#endif // !defined(AFX_TIMER_H__9C0AECD0_E214_4EF0_AFF8_C2500677CACD__INCLUDED_)

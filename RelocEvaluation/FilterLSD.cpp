#include "FilterLSD.h"

using namespace cv;


FilterLSD::FilterLSD() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSD::FilterLSD(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSD::~FilterLSD()
{
}

void FilterLSD::calcFeature(){
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	//if (descriptor.cols == 0) {
	//	descriptor = cv::Mat::zeros(Size(200, 1), CV_32S);
	//}
	Mat len = cv::Mat::zeros(cv::Size(lines.size(), 1), CV_32F);
	vector<float> lengths((int)lines.size(), 0);
	vector<float> angles((int)lines.size(), 0);
	for (int n = 0; n < (int)lines.size(); ++n){
		lengths[n] = sqrt((lines[n][0] - lines[n][2])*(lines[n][0] - lines[n][2]) + (lines[n][1] - lines[n][3])*(lines[n][1] - lines[n][3]));
		len.at<float>(0, n) = sqrt((lines[n][0] - lines[n][2])*(lines[n][0] - lines[n][2]) + (lines[n][1] - lines[n][3])*(lines[n][1] - lines[n][3]));
		angles[n] = atan2f((lines[n][1] - lines[n][3]), (lines[n][0] - lines[n][2]));
	}

	int histSize[] = { 100 };
	float range[] = { 0, 500 };
	const float* ranges[] = { range };
	int channels[] = { 0 };
	calcHist(&len, 1, channels, Mat(), descriptor, 1, histSize, ranges, true, true);
	
	double max_val = .0;
	minMaxLoc(descriptor, 0, &max_val);

	const int ch_width = 1280;
	Mat hist_img(Size(ch_width, 640), CV_8UC3, Scalar::all(255));
	Scalar color = Scalar::all(100);
	descriptor.convertTo(len, CV_32F, max_val ? 600 / max_val : 0., 0);
	for (int j = 0; j < 100; ++j) {
		int bin_w = saturate_cast<int>((double)ch_width / 100);
		rectangle(hist_img,
			Point(j*bin_w, hist_img.rows),
			Point((j + 1)*bin_w, hist_img.rows - saturate_cast<int>(len.at<float>(j, 0))),
			color, -1);
	}

	imshow("hist", hist_img);
	waitKey(1);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSD)
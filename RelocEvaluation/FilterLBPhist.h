#pragma once
#include "Filter.h"
class FilterLBPhist :
	public Filter
{
public:
	FilterLBPhist();
	FilterLBPhist(int);
	FilterLBPhist(Method);
	FilterLBPhist(int, Method);
	FilterLBPhist(cv::Size);
	FilterLBPhist(int, cv::Size);
	FilterLBPhist(Method, cv::Size);
	FilterLBPhist(int, Method, cv::Size);
	FilterLBPhist(filter);
	FilterLBPhist(int, filter);
	FilterLBPhist(ImageIO* imgIO);
	FilterLBPhist(ImageIO* imgIO, int);
	FilterLBPhist(ImageIO* imgIO, Method);
	FilterLBPhist(ImageIO* imgIO, int, Method);
	FilterLBPhist(ImageIO* imgIO, cv::Size);
	FilterLBPhist(ImageIO* imgIO, int, cv::Size);
	FilterLBPhist(ImageIO* imgIO, Method, cv::Size);
	FilterLBPhist(ImageIO* imgIO, int, Method, cv::Size);
	FilterLBPhist(ImageIO* imgIO, filter);
	FilterLBPhist(ImageIO* imgIO, int, filter);
	~FilterLBPhist();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	void clearDB(){
		descriptors.clear();
	}
	vector<cv::Mat> descriptors;
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLBPhist)

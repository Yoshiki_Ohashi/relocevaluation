#pragma once
#include "Filter.h"

class FilterLSDsolo :
	public Filter
{
public:
	FilterLSDsolo();
	FilterLSDsolo(int);
	FilterLSDsolo(Method);
	FilterLSDsolo(int, Method);
	FilterLSDsolo(cv::Size);
	FilterLSDsolo(int, cv::Size);
	FilterLSDsolo(Method, cv::Size);
	FilterLSDsolo(int, Method, cv::Size);
	FilterLSDsolo(filter);
	FilterLSDsolo(int, filter);
	FilterLSDsolo(ImageIO* imgIO);
	FilterLSDsolo(ImageIO* imgIO, int);
	FilterLSDsolo(ImageIO* imgIO, Method);
	FilterLSDsolo(ImageIO* imgIO, int, Method);
	FilterLSDsolo(ImageIO* imgIO, cv::Size);
	FilterLSDsolo(ImageIO* imgIO, int, cv::Size);
	FilterLSDsolo(ImageIO* imgIO, Method, cv::Size);
	FilterLSDsolo(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDsolo(ImageIO* imgIO, filter);
	FilterLSDsolo(ImageIO* imgIO, int, filter);
	~FilterLSDsolo();

	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	const double len_th1 = 25; //5^2
	const double len_th2 = 100; //10^2
	const double len_th3 = 400; //20^2

	const float horizontal_th = tanf(CV_PI / 18.);
	const float vertical_th = tanf(CV_PI*4. / 9.);

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDsolo)
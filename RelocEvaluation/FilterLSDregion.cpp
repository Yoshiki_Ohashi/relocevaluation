#include "FilterLSDregion.h"

using namespace cv;


FilterLSDregion::FilterLSDregion() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDregion::FilterLSDregion(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDregion::~FilterLSDregion()
{
}

void FilterLSDregion::calcFeature(){
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = cv::Mat::zeros(cv::Size(4, 1), CV_32S);
	int* des = descriptor.ptr<int>(0);
	for (int n = 0; n < (int)lines.size(); ++n) {
		if ((lines[n][0] + lines[n][2]) < size.width) {
			if ((lines[n][1] + lines[n][3]) < size.height) des[0] += 1;
			else des[1] += 1;
		}
		else {
			if ((lines[n][1] + lines[n][3]) < size.height) des[2] += 1;
			else des[3] += 1;
		}
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDregion)
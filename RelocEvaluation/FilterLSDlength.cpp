#include "FilterLSDlength.h"

using namespace cv;


FilterLSDlength::FilterLSDlength() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDlength::FilterLSDlength(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDlength::~FilterLSDlength()
{
}

void FilterLSDlength::calcFeature() {
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = cv::Mat::zeros(cv::Size(4, 1), CV_32S);
	int* des = descriptor.ptr<int>(0);
	double length;
	for (int n = 0; n < (int)lines.size(); ++n) {
		length = (lines[n][0] - lines[n][2])*(lines[n][0] - lines[n][2]) + (lines[n][1] - lines[n][3])*(lines[n][1] - lines[n][3]);
		if (length < th2) {
			if (length < th1) des[0] += 1;
			else des[1] += 1;
		}
		else {
			if (length < th3) des[2] += 1;
			else des[3] += 1;
		}
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDlength)
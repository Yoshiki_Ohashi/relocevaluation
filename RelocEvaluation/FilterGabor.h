#pragma once
#include "Filter.h"
#include <vector>
class FilterGabor :
	public Filter
{
public:
	FilterGabor();
	FilterGabor(int);
	FilterGabor(Method);
	FilterGabor(int, Method);
	FilterGabor(cv::Size);
	FilterGabor(int, cv::Size);
	FilterGabor(Method, cv::Size);
	FilterGabor(int, Method, cv::Size);
	FilterGabor(filter);
	FilterGabor(int, filter);
	FilterGabor(ImageIO* imgIO);
	FilterGabor(ImageIO* imgIO, int);
	FilterGabor(ImageIO* imgIO, Method);
	FilterGabor(ImageIO* imgIO, int, Method);
	FilterGabor(ImageIO* imgIO, cv::Size);
	FilterGabor(ImageIO* imgIO, int, cv::Size);
	FilterGabor(ImageIO* imgIO, Method, cv::Size);
	FilterGabor(ImageIO* imgIO, int, Method, cv::Size);
	FilterGabor(ImageIO* imgIO, filter);
	FilterGabor(ImageIO* imgIO, int, filter);
	~FilterGabor();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}

	cv::Mat gabor0;
	cv::Mat gabor45;
	cv::Mat gabor90;
	cv::Mat gabor135; 
	
	const int ksize = 9;
	const double sigma = 3;
	const double lambda = CV_PI;
	const double gamma = 0.3;
	const double psi = 0;

	const cv::Mat filter0 = getGaborKernel(cv::Size(ksize, ksize), sigma, 0, lambda, gamma, psi, CV_32F);
	const cv::Mat filter45 = getGaborKernel(cv::Size(ksize, ksize), sigma, CV_PI / 4.0, lambda, gamma, psi, CV_32F);
	const cv::Mat filter90 = getGaborKernel(cv::Size(ksize, ksize), sigma, CV_PI / 2.0, lambda, gamma, psi, CV_32F);
	const cv::Mat filter135 = getGaborKernel(cv::Size(ksize, ksize), sigma, -CV_PI / 4.0, lambda, gamma, psi, CV_32F);


	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterGabor)
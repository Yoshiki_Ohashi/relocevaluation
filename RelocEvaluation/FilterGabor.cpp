#include "FilterGabor.h"

using namespace cv;


FilterGabor::FilterGabor() : Filter(){

}

FilterGabor::FilterGabor(int thread) : Filter(thread){

}

FilterGabor::FilterGabor(Method method) : Filter(method){

}

FilterGabor::FilterGabor(int thread, Method method) : Filter(thread, method){

}

FilterGabor::FilterGabor(Size size) : Filter(size){

}

FilterGabor::FilterGabor(int thread, Size size) : Filter(thread, size){

}

FilterGabor::FilterGabor(Method method, Size size) : Filter(method, size){

}

FilterGabor::FilterGabor(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterGabor::FilterGabor(filter filter) : Filter(filter){

}

FilterGabor::FilterGabor(int thread, filter filter) : Filter(thread, filter){

}

FilterGabor::FilterGabor(ImageIO* imgIO) : Filter(imgIO){

}

FilterGabor::FilterGabor(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterGabor::FilterGabor(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterGabor::FilterGabor(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterGabor::FilterGabor(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterGabor::FilterGabor(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterGabor::FilterGabor(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterGabor::FilterGabor(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterGabor::FilterGabor(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterGabor::FilterGabor(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}

FilterGabor::~FilterGabor()
{
}

void FilterGabor::calcFeature(){
	Mat src = ImgIO->getResizedGRAY(thread);

	filter2D(src, gabor0, CV_32F, filter0, Point(-1, -1), 0.0, 4);
	filter2D(src, gabor45, CV_32F, filter45, Point(-1, -1), 0.0, 4);
	filter2D(src, gabor90, CV_32F, filter90, Point(-1, -1), 0.0, 4);
	filter2D(src, gabor135, CV_32F, filter135, Point(-1, -1), 0.0, 4);
								  
	double minVal, maxVal;
	cv::Point minLoc, maxLoc;
	cv::minMaxLoc(gabor0, &minVal, &maxVal, &minLoc, &maxLoc);
	gabor0 = (gabor0 - minVal) / (maxVal - minVal) * 255;
	cv::minMaxLoc(gabor45, &minVal, &maxVal, &minLoc, &maxLoc);
	gabor45 = (gabor45 - minVal) / (maxVal - minVal) * 255;
	cv::minMaxLoc(gabor90, &minVal, &maxVal, &minLoc, &maxLoc);
	gabor90 = (gabor90 - minVal) / (maxVal - minVal) * 255;
	cv::minMaxLoc(gabor135, &minVal, &maxVal, &minLoc, &maxLoc);
	gabor135 = (gabor135 - minVal) / (maxVal - minVal) * 255;

	gabor0.convertTo(gabor0, CV_8U);
	gabor45.convertTo(gabor45, CV_8U);
	gabor90.convertTo(gabor90, CV_8U);
	gabor135.convertTo(gabor135, CV_8U);

	Mat hist[4];
	int histSize[] = { 16 };
	float range[] = { 0, 256 };
	const float* ranges[] = { range };
	int channels[] = { 0 };
	calcHist(&gabor0, 1, channels, Mat(), hist[0], 1, histSize, ranges, true, false);
	calcHist(&gabor45, 1, channels, Mat(), hist[1], 1, histSize, ranges, true, false);
	calcHist(&gabor90, 1, channels, Mat(), hist[2], 1, histSize, ranges, true, false);
	calcHist(&gabor135, 1, channels, Mat(), hist[3], 1, histSize, ranges, true, false);

	vconcat(hist, 4, descriptor);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterGabor)

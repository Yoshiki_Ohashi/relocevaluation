#pragma once

//#define Fugen
#define Seven_Scenes

#ifdef Fugen
#define CameraPoseIsKnown false
#else
#define CameraPoseIsKnown true
#endif

#define threshAngle (CV_PI/9.0)
#define threshTrans (pow(0.20,2))

#define NumThreads 3
#define NumSelect 3 //Number of selected combination for each optimization.

#ifdef Fugen
#define DBsize 15
#else
#ifdef Seven_Scenes
#define DBsize 26000
#else
#define DBsize 0
#endif
#endif

#define RequiredTime 1000 //msec DB作成〜評価までを行うかどうかを判断する粗めの閾値
#define FinalRequiredTime 300 //msec 最終的に許容範囲内かを判断する閾値

#ifdef Fugen
#define CuttingRatio 3295 // about 1 image per CuttingRatio images will pass a filter
#else
#ifdef Seven_Scenes
#define CuttingRatio 14 // about 1 image per CuttingRatio images will pass a filter
#else
#define CuttingRatio 10 // about 1 image per CuttingRatio images will pass a filter
#endif
#endif

#ifdef Fugen
#define nameRGB(name, ID) sprintf_s(name, "images\\%05d.color.png", ID)
#define nameDEPTH(name, ID) sprintf_s(name, "images\\%05d.depth.png", ID)
#else
#define nameRGB(name, ID) sprintf_s(name, "%06d.color.png", ID)
#define nameDEPTH(name, ID) sprintf_s(name, "%06d.depth.png", ID)
#define namePOSE(name, ID) sprintf_s(name, "%06d.txt", ID)
#endif

#define numFerns 500

#define NumTrainingImages 1000

#define DictionarySize 200
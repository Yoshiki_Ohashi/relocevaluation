#include "FilterRForiginal.h"
using namespace cv;


FilterRForiginal::FilterRForiginal() : Filter() {

}

FilterRForiginal::FilterRForiginal(int thread) : Filter(thread) {

}

FilterRForiginal::FilterRForiginal(Method method) : Filter(method) {

}

FilterRForiginal::FilterRForiginal(int thread, Method method) : Filter(thread, method) {

}

FilterRForiginal::FilterRForiginal(Size size) : Filter(size) {

}

FilterRForiginal::FilterRForiginal(int thread, Size size) : Filter(thread, size) {

}

FilterRForiginal::FilterRForiginal(Method method, Size size) : Filter(method, size) {

}

FilterRForiginal::FilterRForiginal(int thread, Method method, Size size) : Filter(thread, method, size) {

}

FilterRForiginal::FilterRForiginal(filter filter) : Filter(filter) {

}

FilterRForiginal::FilterRForiginal(int thread, filter filter) : Filter(thread, filter) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO) : Filter(imgIO) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, int thread) : Filter(imgIO, thread) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, Method method) : Filter(imgIO, method) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, Size size) : Filter(imgIO, size) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, filter filter) : Filter(imgIO, filter) {

}

FilterRForiginal::FilterRForiginal(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter) {

}

FilterRForiginal::~FilterRForiginal()
{
	delete[] positions;
	delete[] thresholds;
	delete[] pPositions;
	delete[] tables;
	for (int i = 0; i < filledDB; ++i) {
		delete[] descriptors[i];
	}
}

bool FilterRForiginal::checkSpeed(int repeat) {
	if (!SpeedIsChecked) {
		randomizePosAndThresh();
		init();
		random_device rnd;
		uniform_int_distribution<> random(0, DBsize - 1);
		makeDB(repeat);
		timePrepare = 0;
		timeCompare = 0;
		for (int i = 0; i < repeat; i++) {
			ImgIO->read(random(rnd));
			screening(repeat, &timePrepare, &timeCompare);
		}
		clearDB();
		timePrepare = timePrepare * 1000 / (double)repeat;
		timeCompare = timeCompare * 1000 / (double)repeat / (double)repeat;
		SpeedIsChecked = true;
	}
	double time = timePrepare + timeCompare*candidateSize;
	if (time < requiredTime) return true;
	else return false;
}

void FilterRForiginal::makeDB(){
	randomizePosAndThresh();
	allocMat();
	getPointers();
	hasToCount = false;
	for (int ID = 0; ID < DBsize; ID++) {
		extractFeature(ID);
		addIDtoTable(ID);
	}
}

void FilterRForiginal::makeDB(int repeat) {
	random_device rnd;
	uniform_int_distribution<> random16(0, 15);
	int code;
	for (int i = 0; i < DBsize; ++i) {
		for (int j = 0; j < numFerns; ++j) {
			code = random16(rnd);
			tables[16 * j + code].push_back(i);
		}
	}
}

void FilterRForiginal::randomizePosAndThresh() {
	int n = size.width * size.height * 3;
	vector<int> pixels;
	generateUniqueRandomArray(pixels, 4 * numFerns, 0, n - 1);
	for (int i = 0; i < 4 * numFerns; ++i) {
		positions[i] = pixels[i];
	}

	thresholds = new uchar[4 * numFerns];
	random_device rnd;
	uniform_int_distribution<> rand256(0, 255);
	for (int i = 0; i < 4 * numFerns; ++i) {
		thresholds[i] = (uchar)rand256(rnd);
	}
}

void FilterRForiginal::allocMat() {
	blurred = Mat::zeros(size, CV_32FC3);
}

void FilterRForiginal::getPointers() {
	//float* data = (float*)blurred.data;
	for (int i = 0; i < 4 * numFerns; ++i) {
		if(true) pPositions[i] = (float*)blurred.data + positions[i];
	}
}

void FilterRForiginal::calcFeature() {
	filter2D(ImgIO->getResizedRGB(thread), blurred, CV_32F, kernel);
	for (int i = 0; i < numFerns; ++i) {
		descriptor[i] = 0;
		for (int j = 0; j < 4; ++j) {
			if (*(pPositions[4 * i + j]) < thresholds[4 * i + j]) {
				descriptor[i] |= 1 << j;
			}
		}
	}
	if (hasToCount) countCooccurrence();
}

void FilterRForiginal::addIDtoTable(int ID) {
	for (int i = 0; i < numFerns; ++i) {
		tables[16 * i + descriptor[i]].push_back(ID);
	}
}

void FilterRForiginal::addFeatureToDB(){
	descriptors[filledDB] = new uchar[numFerns];
	memcpy(descriptors[filledDB], descriptor, numFerns * sizeof(uchar));
	filledDB++;
}

void FilterRForiginal::countCooccurrence() {
	for (int i = 0; i < DBsize; ++i) {
		coocurrence[i] = 0;
	}

	for (int i = 0; i < numFerns; ++i) {
		int tbl = 16 * i + descriptor[i];
		int jmax = tables[tbl].size();
		for (int j = 0; j < jmax; ++j) {
			coocurrence[(tables[tbl])[j]] += 1;
		}
	}
}

void FilterRForiginal::generateUniqueRandomArray(vector<int>& vec, const int numElements, const int rangeMIN, const int rangeMAX) {
	int margin = 1.2 * numElements;
	vec.clear();
	vec.reserve(margin);
	random_device rand;
	uniform_int_distribution<> distribution(rangeMIN, rangeMAX);
	while (vec.size() < numElements) {
		while (vec.size() < margin) vec.push_back(distribution(rand));
		std::sort(vec.begin(), vec.end());
		auto unique_end = std::unique(vec.begin(), vec.end());

		if (numElements < std::distance(vec.begin(), unique_end)) {
			unique_end = std::next(vec.begin(), numElements);
		}
		vec.erase(unique_end, vec.end());
	}
	for (int i = 0; i < numElements - 1; ++i) {
		uniform_int_distribution<> dist(i, numElements - 1);
		int rnd = dist(rand);
		int tmp = vec[i];
		vec[i] = vec[rnd];
		vec[rnd] = tmp;
	}
}

void FilterRForiginal::init() {
	allocMat();
	getPointers();
	hasToCount = true;
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterRForiginal)
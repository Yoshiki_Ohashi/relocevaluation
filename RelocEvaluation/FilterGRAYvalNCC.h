#pragma once
#include "Filter.h"

extern double NCC(const cv::Mat img1, const cv::Mat img2);

class FilterGRAYvalNCC :
	public Filter
{
public:
	FilterGRAYvalNCC();
	FilterGRAYvalNCC(int);
	FilterGRAYvalNCC(Method);
	FilterGRAYvalNCC(int, Method);
	FilterGRAYvalNCC(cv::Size);
	FilterGRAYvalNCC(int, cv::Size);
	FilterGRAYvalNCC(Method, cv::Size);
	FilterGRAYvalNCC(int, Method, cv::Size);
	FilterGRAYvalNCC(filter);
	FilterGRAYvalNCC(int, filter);
	FilterGRAYvalNCC(ImageIO* imgIO);
	FilterGRAYvalNCC(ImageIO* imgIO, int);
	FilterGRAYvalNCC(ImageIO* imgIO, Method);
	FilterGRAYvalNCC(ImageIO* imgIO, int, Method);
	FilterGRAYvalNCC(ImageIO* imgIO, cv::Size);
	FilterGRAYvalNCC(ImageIO* imgIO, int, cv::Size);
	FilterGRAYvalNCC(ImageIO* imgIO, Method, cv::Size);
	FilterGRAYvalNCC(ImageIO* imgIO, int, Method, cv::Size);
	FilterGRAYvalNCC(ImageIO* imgIO, filter);
	FilterGRAYvalNCC(ImageIO* imgIO, int, filter);
	~FilterGRAYvalNCC();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature(){
		descriptor = ImgIO->getResizedGRAY(thread);
	}
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return 1.0 - NCC(descriptor, descriptors[ID]);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterGRAYvalNCC)

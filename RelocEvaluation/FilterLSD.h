#pragma once
#include "Filter.h"

#define Num_Bins_X 2
#define Num_Bins_Y 2
#define Num_Bins_Length 4 
#define Num_Bins_Angle 4

class FilterLSD :
	public Filter
{
public:
	FilterLSD();
	FilterLSD(int);
	FilterLSD(Method);
	FilterLSD(int, Method);
	FilterLSD(cv::Size);
	FilterLSD(int, cv::Size);
	FilterLSD(Method, cv::Size);
	FilterLSD(int, Method, cv::Size);
	FilterLSD(filter);
	FilterLSD(int, filter);
	FilterLSD(ImageIO* imgIO);
	FilterLSD(ImageIO* imgIO, int);
	FilterLSD(ImageIO* imgIO, Method);
	FilterLSD(ImageIO* imgIO, int, Method);
	FilterLSD(ImageIO* imgIO, cv::Size);
	FilterLSD(ImageIO* imgIO, int, cv::Size);
	FilterLSD(ImageIO* imgIO, Method, cv::Size);
	FilterLSD(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSD(ImageIO* imgIO, filter);
	FilterLSD(ImageIO* imgIO, int, filter);
	~FilterLSD();

	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSD)
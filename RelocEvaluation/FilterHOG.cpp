#include "FilterHOG.h"


using namespace cv;


FilterHOG::FilterHOG() : Filter(){

}

FilterHOG::FilterHOG(int thread) : Filter(thread){

}

FilterHOG::FilterHOG(Method method) : Filter(method){

}

FilterHOG::FilterHOG(int thread, Method method) : Filter(thread, method){

}

FilterHOG::FilterHOG(Size size) : Filter(size){

}

FilterHOG::FilterHOG(int thread, Size size) : Filter(thread, size){

}

FilterHOG::FilterHOG(Method method, Size size) : Filter(method, size){

}

FilterHOG::FilterHOG(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterHOG::FilterHOG(filter filter) : Filter(filter){

}

FilterHOG::FilterHOG(int thread, filter filter) : Filter(thread, filter){

}

FilterHOG::FilterHOG(ImageIO* imgIO) : Filter(imgIO){

}

FilterHOG::FilterHOG(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterHOG::FilterHOG(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterHOG::FilterHOG(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterHOG::FilterHOG(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterHOG::FilterHOG(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterHOG::FilterHOG(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterHOG::FilterHOG(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterHOG::FilterHOG(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterHOG::FilterHOG(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}

FilterHOG::~FilterHOG()
{
}

void FilterHOG::calcFeature(){
	//勾配を計算
	cv::Mat gx, gy;
	cv::Sobel(ImgIO->getResizedGRAY(thread), gx, CV_32F, 1, 0, 1);
	cv::Sobel(ImgIO->getResizedGRAY(thread), gy, CV_32F, 0, 1, 1);

	//勾配ベクトルの大きさと角度を計算
	cv::Mat mag, angle;
	cv::cartToPolar(gx, gy, mag, angle, 1);

	//角度が0〜180度に収まるように再計算
	cv::convertScaleAbs(angle, angle, 1.0, -180);

	const int cols = mag.cols;
	const int rows = mag.rows;
	vector<cv::Mat> cells(cols * rows / 64, cv::Mat::zeros(Size(9, 1), CV_32F));
	float *ptrf;
	uchar *ptru;

	//8x8ピクセルのセルごとに勾配方向のヒストグラムを計算
	for (int row = 0; row < rows; ++row){
		ptrf = mag.ptr<float>(row);
		ptru = angle.ptr<uchar>(row);
		for (int col = 0; col < cols; ++col){
			int cell = (cols / 8)*(row / 8) + (col / 8);
			int bin = int(ptru[col]) / 20;
			float ratio = ptru[col] / 20.0 - bin;
			switch (bin){
			case 0:
				cells[cell].at<float>(0, 0) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 1) += ratio*ptrf[col];
				break;
			case 1:
				cells[cell].at<float>(0, 1) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 2) += ratio*ptrf[col];
				break;
			case 2:
				cells[cell].at<float>(0, 2) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 3) += ratio*ptrf[col];
				break;
			case 3:
				cells[cell].at<float>(0, 3) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 4) += ratio*ptrf[col];
				break;
			case 4:
				cells[cell].at<float>(0, 4) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 5) += ratio*ptrf[col];
				break;
			case 5:
				cells[cell].at<float>(0, 5) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 6) += ratio*ptrf[col];
				break;
			case 6:
				cells[cell].at<float>(0, 6) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 7) += ratio*ptrf[col];
				break;
			case 7:
				cells[cell].at<float>(0, 7) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 8) += ratio*ptrf[col];
				break;
			case 8:
				cells[cell].at<float>(0, 8) += (1.0 - ratio)*ptrf[col];
				cells[cell].at<float>(0, 0) += ratio*ptrf[col];
				break;
			}
		}
	}

	vector<cv::Mat> blocks((cols / 8 - 1)*(rows / 8 - 1));

	//2x2セルのブロックごとにヒストグラムを正規化
	for (int row = 0; row < rows / 8 - 1; ++row){
		for (int col = 0; col < cols / 8 - 1; ++col){
			int cell, block;
			cell = block = (cols / 8 - 1)*row + col;
			vconcat(vector<cv::Mat>{ cells[cell], cells[cell + 1], cells[cell + cols / 8], cells[cell + cols / 8 + 1] }, blocks[block]);
			blocks[block] /= norm(blocks[block], cv::NORM_L2);
		}
	}

	hconcat(blocks, descriptor);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterHOG)
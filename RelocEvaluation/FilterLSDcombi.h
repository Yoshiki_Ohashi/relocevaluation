#pragma once
#include "Filter.h"

class FilterLSDcombi :
	public Filter
{
public:
	FilterLSDcombi();
	FilterLSDcombi(int);
	FilterLSDcombi(Method);
	FilterLSDcombi(int, Method);
	FilterLSDcombi(cv::Size);
	FilterLSDcombi(int, cv::Size);
	FilterLSDcombi(Method, cv::Size);
	FilterLSDcombi(int, Method, cv::Size);
	FilterLSDcombi(filter);
	FilterLSDcombi(int, filter);
	FilterLSDcombi(ImageIO* imgIO);
	FilterLSDcombi(ImageIO* imgIO, int);
	FilterLSDcombi(ImageIO* imgIO, Method);
	FilterLSDcombi(ImageIO* imgIO, int, Method);
	FilterLSDcombi(ImageIO* imgIO, cv::Size);
	FilterLSDcombi(ImageIO* imgIO, int, cv::Size);
	FilterLSDcombi(ImageIO* imgIO, Method, cv::Size);
	FilterLSDcombi(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDcombi(ImageIO* imgIO, filter);
	FilterLSDcombi(ImageIO* imgIO, int, filter);
	~FilterLSDcombi();

	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	const double dis_th1 = 25; //5^2
	const double dis_th2 = 100; //10^2
	const double dis_th3 = 400; //20^2
	const double dis_th4 = 1600; //40^2

	const float ang_th1 = cosf(CV_PI / 8.)*cosf(CV_PI / 8.);
	const float ang_th2 = cosf(CV_PI / 4.)*cosf(CV_PI / 4.);
	const float ang_th3 = cosf(CV_PI*3. / 8.)*cosf(CV_PI*3. / 8.);

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDcombi)
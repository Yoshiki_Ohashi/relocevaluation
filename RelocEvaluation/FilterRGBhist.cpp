#include "FilterRGBhist.h"

using namespace cv;


FilterRGBhist::FilterRGBhist() : Filter(){

}

FilterRGBhist::FilterRGBhist(int thread) : Filter(thread){

}

FilterRGBhist::FilterRGBhist(Method method) : Filter(method){

}

FilterRGBhist::FilterRGBhist(int thread, Method method) : Filter(thread, method){

}

FilterRGBhist::FilterRGBhist(Size size) : Filter(size){

}

FilterRGBhist::FilterRGBhist(int thread, Size size) : Filter(thread, size){

}

FilterRGBhist::FilterRGBhist(Method method, Size size) : Filter(method, size){

}

FilterRGBhist::FilterRGBhist(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterRGBhist::FilterRGBhist(filter filter) : Filter(filter){

}

FilterRGBhist::FilterRGBhist(int thread, filter filter) : Filter(thread, filter){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO) : Filter(imgIO){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterRGBhist::FilterRGBhist(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}


FilterRGBhist::~FilterRGBhist()
{
}

void FilterRGBhist::calcFeature(){
	int histSize[] = { 32 };
	float range[] = { 0, 256 };
	const float* ranges[] = { range };
	cv::Mat hist[3];
	for (int i = 0; i < 3; i++){
		calcHist(&ImgIO->getResizedRGB(thread), 1, &i, Mat(), hist[i], 1, histSize, ranges, true, false);
	}
	cv::hconcat(hist, 3, descriptor);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterRGBhist)
#pragma once

#include <opencv2/features2d.hpp>

namespace cv {

	class BOWKmajorityTrainer : public BOWTrainer {

	public:
		BOWKmajorityTrainer(int clusterCount, const TermCriteria& termcrit = TermCriteria());
		virtual ~BOWKmajorityTrainer();
		virtual Mat cluster() const;
		virtual Mat cluster(const Mat& descriptors) const;

	protected:
		int numClusters;
		int maxIterations;
	};

}
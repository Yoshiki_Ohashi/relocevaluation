#include "FilterSURFmatch.h"

using namespace cv;


FilterSURFmatch::FilterSURFmatch() : Filter(){
}

FilterSURFmatch::FilterSURFmatch(int thread) : Filter(thread){
}

FilterSURFmatch::FilterSURFmatch(Method method) : Filter(method){
}

FilterSURFmatch::FilterSURFmatch(int thread, Method method) : Filter(thread, method){
}

FilterSURFmatch::FilterSURFmatch(Size size) : Filter(size){
}

FilterSURFmatch::FilterSURFmatch(int thread, Size size) : Filter(thread, size) {
}

FilterSURFmatch::FilterSURFmatch(Method method, Size size) : Filter(method, size){
}

FilterSURFmatch::FilterSURFmatch(int thread, Method method, Size size) : Filter(thread, method, size){
}

FilterSURFmatch::FilterSURFmatch(filter filter) : Filter(filter){
}

FilterSURFmatch::FilterSURFmatch(int thread, filter filter) : Filter(thread, filter){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO) : Filter(imgIO){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, Method method) : Filter(imgIO, method){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, Size size) : Filter(imgIO, size){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
}

FilterSURFmatch::FilterSURFmatch(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
}

FilterSURFmatch::~FilterSURFmatch()
{
}

void FilterSURFmatch::calcFeature(){
	Mat src = ImgIO->getResizedGRAY(thread);
	detector->detect(src, keypoints);
	if (keypoints.size() > 200) {
		std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);
		keypoints.erase(keypoints.begin() + 200, keypoints.end());
	}
	detector->compute(src, keypoints, descriptor);
}

double FilterSURFmatch::compare(int ID) {
	vector<cv::DMatch> dmatch, dmatch1, dmatch2;

	matcher->match(descriptor, descriptors[ID], dmatch1);
	matcher->match(descriptors[ID], descriptor, dmatch2);

	for (size_t i = 0; i < dmatch1.size(); ++i)
	{
		cv::DMatch forward = dmatch1[i];
		cv::DMatch backward = dmatch2[forward.trainIdx];
		if (backward.trainIdx == forward.queryIdx)
			dmatch.push_back(forward);
	}

	return ((double)dmatch1.size() - (double)dmatch.size()) / (double)descriptors.rows;

	//double avgDistance = 0;
	//int numMatch = (int)dmatch.size();

	//if (numMatch == 0) {
	//	return DBL_MAX;
	//}

	//for (int i = 0; i < numMatch; ++i) {
	//	Point2f keypoint1 = keypoints[dmatch[i].queryIdx].pt;
	//	Point2f keypoint2 = vecKeypoints[ID][dmatch[i].trainIdx].pt;
	//	Point2f difference = keypoint1 - keypoint2;
	//	avgDistance += sqrt(difference.x*difference.x + difference.y*difference.y);
	//}

	//avgDistance /= numMatch;

	//return avgDistance;
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterSURFmatch)
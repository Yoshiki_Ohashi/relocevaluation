#include "FilterLSDsolo.h"

using namespace cv;


FilterLSDsolo::FilterLSDsolo() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::FilterLSDsolo(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDsolo::~FilterLSDsolo()
{
}

void FilterLSDsolo::calcFeature() {
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = cv::Mat::zeros(cv::Size(32, 1), CV_32S);
	int* des = descriptor.ptr<int>(0);
	double length;
	double angle;
	int bin;
	for (int n = 0; n < (int)lines.size(); ++n) {
		length = (lines[n][0] - lines[n][2])*(lines[n][0] - lines[n][2]) + (lines[n][1] - lines[n][3])*(lines[n][1] - lines[n][3]);
		angle = abs((lines[n][1] - lines[n][3]) / (lines[n][0] - lines[n][2]));
		if ((lines[n][0] + lines[n][2]) < size.width) {
			if ((lines[n][1] + lines[n][3]) < size.height) bin = 0;
			else bin = 8;
		}
		else {
			if ((lines[n][1] + lines[n][3]) < size.height) bin = 16;
			else bin = 24;
		}
		if (length < len_th2) {
			if (length >= len_th1) bin += 2;
		}
		else {
			if (length < len_th3) bin += 4;
			else bin += 6;
		}
		if (angle > vertical_th) des[bin] += 1;
		if (angle < horizontal_th) des[bin + 1] += 1;
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDsolo)
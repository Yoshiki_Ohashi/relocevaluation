#pragma once

#include <opencv2/opencv.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/vector.hpp>

BOOST_SERIALIZATION_SPLIT_FREE(::cv::Mat)
namespace boost {
	namespace serialization {

		/** Serialization support for cv::Mat */
		template<class Archive>
		void save(Archive& ar, const ::cv::Mat& m, const unsigned int version)
		{
			size_t elem_size = m.elemSize();
			size_t elem_type = m.type();

			ar & m.cols;
			ar & m.rows;
			ar & elem_size;
			ar & elem_type;

			const size_t data_size = m.cols * m.rows * elem_size;
			ar & boost::serialization::make_array(m.ptr(), data_size);
		}

		/** Serialization support for cv::Mat */
		template<class Archive>
		void load(Archive& ar, ::cv::Mat& m, const unsigned int version)
		{
			int cols, rows;
			size_t elem_size, elem_type;

			ar & cols;
			ar & rows;
			ar & elem_size;
			ar & elem_type;

			m.create(rows, cols, (int)elem_type);

			size_t data_size = m.cols * m.rows * elem_size;
			ar & boost::serialization::make_array(m.ptr(), data_size);
		}

	}
}

BOOST_SERIALIZATION_SPLIT_FREE(::cv::Vec4f)
namespace boost {
	namespace serialization {

		template<class Archive>
		void save(Archive& ar, const ::cv::Vec4f& v, const unsigned int version)
		{
			ar & v.val;
		}

		template<class Archive>
		void load(Archive& ar, ::cv::Vec4f& v, const unsigned int version)
		{
			ar & v.val;
		}
	}
}

BOOST_SERIALIZATION_SPLIT_FREE(::cv::Point2f)
namespace boost {
	namespace serialization {

		template<class Archive>
		void save(Archive& ar, const ::cv::Point2f& p, const unsigned int version)
		{
			ar & p.x;
			ar & p.y;
		}

		template<class Archive>
		void load(Archive& ar, ::cv::Point2f& p, const unsigned int version)
		{
			ar & p.x;
			ar & p.y;
		}
	}
}

BOOST_SERIALIZATION_SPLIT_FREE(::cv::KeyPoint)
namespace boost {
	namespace serialization {

		template<class Archive>
		void save(Archive& ar, const ::cv::KeyPoint& p, const unsigned int version)
		{
			ar & p.angle;
			ar & p.class_id;
			ar & p.octave;
			ar & p.pt;
			ar & p.response;
			ar & p.size;
		}

		template<class Archive>
		void load(Archive& ar, ::cv::KeyPoint& p, const unsigned int version)
		{
			ar & p.angle;
			ar & p.class_id;
			ar & p.octave;
			ar & p.pt;
			ar & p.response;
			ar & p.size;
		}
	}
}
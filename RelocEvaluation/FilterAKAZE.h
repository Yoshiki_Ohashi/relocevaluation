#pragma once
#include "Filter.h"
#include "BOWKmajorityTrainer.h"

class FilterAKAZE :
	public Filter
{
public:
	FilterAKAZE();
	FilterAKAZE(int);
	FilterAKAZE(Method);
	FilterAKAZE(int, Method);
	FilterAKAZE(cv::Size);
	FilterAKAZE(int, cv::Size);
	FilterAKAZE(Method, cv::Size);
	FilterAKAZE(int, Method, cv::Size);
	FilterAKAZE(filter);
	FilterAKAZE(int, filter);
	FilterAKAZE(ImageIO* imgIO);
	FilterAKAZE(ImageIO* imgIO, int);
	FilterAKAZE(ImageIO* imgIO, Method);
	FilterAKAZE(ImageIO* imgIO, int, Method);
	FilterAKAZE(ImageIO* imgIO, cv::Size);
	FilterAKAZE(ImageIO* imgIO, int, cv::Size);
	FilterAKAZE(ImageIO* imgIO, Method, cv::Size);
	FilterAKAZE(ImageIO* imgIO, int, Method, cv::Size);
	FilterAKAZE(ImageIO* imgIO, filter);
	FilterAKAZE(ImageIO* imgIO, int, filter);
	~FilterAKAZE();

	bool checkSpeed(int);
	void makeDB();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
		ar & vocabulary;
		ar & hasVocabulary;
		ar & hasDB;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	cv::Mat vocabulary;
	cv::Ptr<cv::FeatureDetector> detector;
	cv::Ptr<cv::DescriptorMatcher> matcher;
	vector<cv::KeyPoint> keypoints;
	cv::Ptr<cv::BOWImgDescriptorExtractor> extractor;

	bool hasVocabulary;
	bool hasDB;

	void createDetector();
	void createMatcher();
	void createExtractor();
	void makeVocabulary();

	void makeDB(int);
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromRGB(ID);
	}
	void extractFeature(){
		extractFeatureFromRGB();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterAKAZE)
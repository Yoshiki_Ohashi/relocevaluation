#include "FilterAKAZE.h"

using namespace cv;

FilterAKAZE::FilterAKAZE() : Filter(){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(int thread) : Filter(thread){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(Method method) : Filter(method){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(int thread, Method method) : Filter(thread, method){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(Size size) : Filter(size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(int thread, Size size) : Filter(thread, size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(Method method, Size size) : Filter(method, size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(int thread, Method method, Size size) : Filter(thread, method, size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(filter filter) : Filter(filter){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(int thread, filter filter) : Filter(thread, filter){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO) : Filter(imgIO){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::FilterAKAZE(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	hasVocabulary = false;
	hasDB = false;
}

FilterAKAZE::~FilterAKAZE()
{
}

void FilterAKAZE::createDetector(){
	detector = cv::AKAZE::create();
}

void FilterAKAZE::createMatcher(){
	matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
}

void FilterAKAZE::createExtractor(){
	extractor = new cv::BOWImgDescriptorExtractor(detector, matcher);
}

bool FilterAKAZE::checkSpeed(int repeat){
	createDetector();
	createMatcher();
	createExtractor();
	if (!SpeedIsChecked){
		if (!hasVocabulary){
			makeVocabulary();
		}
		extractor->setVocabulary(vocabulary);
		random_device rnd;
		uniform_int_distribution<> random(0, DBsize - 1);
		makeDB(repeat);
		timePrepare = 0;
		timeCompare = 0;
		for (int i = 0; i < repeat; i++){
			ImgIO->read(random(rnd));
			screening(repeat, &timePrepare, &timeCompare);
		}
		clearDB();
		timePrepare = timePrepare * 1000 / (double)repeat;
		timeCompare = timeCompare * 1000 / (double)repeat / (double)repeat;
		SpeedIsChecked = true;
	}
	double time = timePrepare + timeCompare*candidateSize;
	if (time < requiredTime) return true;
	else return false;
}

void FilterAKAZE::makeDB(){
	createDetector();
	createMatcher();
	createExtractor();

	if (!hasVocabulary) makeVocabulary();
	extractor->setVocabulary(vocabulary); 
	
	if (!hasDB){
		for (int ID = 0; ID < DBsize; ID++){
			extractFeature(ID);
			addFeatureToDB();
		}
		hasDB = true;
	}
}

void FilterAKAZE::makeDB(int repeat){
	random_device rnd;
	uniform_int_distribution<> random(0, DBsize - 1);
	for (int i = 0; i < repeat; i++){
		extractFeature(random(rnd));
		addFeatureToDB();
	}
}

void FilterAKAZE::makeVocabulary(){
	const int increment = DBsize / NumTrainingImages;
	cv::BOWKmajorityTrainer trainer(DictionarySize, cv::TermCriteria(CV_TERMCRIT_ITER, 100, 0.001));
	for (int ID = 0; ID < DBsize; ID += increment){
		ImgIO->read(ID, RGB, size, thread);
		Mat src = ImgIO->getResizedRGB(thread);
		detector->detect(src, keypoints);
		if (keypoints.size() > 0) {
			if (keypoints.size() > 100) {
				std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);
				keypoints.erase(keypoints.begin() + 100, keypoints.end());
			}
			detector->compute(src, keypoints, descriptor);
			trainer.add(descriptor);
		}
	}
	vocabulary = trainer.cluster();
	hasVocabulary = true;
}

void FilterAKAZE::calcFeature(){
	Mat src = ImgIO->getResizedRGB(thread);
	detector->detect(src, keypoints);
	if (keypoints.size() > 500) {
		std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);
		keypoints.erase(keypoints.begin() + 500, keypoints.end());
	}
	if (keypoints.size() == 0) descriptor = Mat::zeros(Size(DictionarySize, 1), CV_32F);
	else extractor->compute(src, keypoints, descriptor);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterAKAZE)

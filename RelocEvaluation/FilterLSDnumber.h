#pragma once
#include "Filter.h"

class FilterLSDnumber :
	public Filter
{
public:
	FilterLSDnumber();
	FilterLSDnumber(int);
	FilterLSDnumber(Method);
	FilterLSDnumber(int, Method);
	FilterLSDnumber(cv::Size);
	FilterLSDnumber(int, cv::Size);
	FilterLSDnumber(Method, cv::Size);
	FilterLSDnumber(int, Method, cv::Size);
	FilterLSDnumber(filter);
	FilterLSDnumber(int, filter);
	FilterLSDnumber(ImageIO* imgIO);
	FilterLSDnumber(ImageIO* imgIO, int);
	FilterLSDnumber(ImageIO* imgIO, Method);
	FilterLSDnumber(ImageIO* imgIO, int, Method);
	FilterLSDnumber(ImageIO* imgIO, cv::Size);
	FilterLSDnumber(ImageIO* imgIO, int, cv::Size);
	FilterLSDnumber(ImageIO* imgIO, Method, cv::Size);
	FilterLSDnumber(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDnumber(ImageIO* imgIO, filter);
	FilterLSDnumber(ImageIO* imgIO, int, filter);
	~FilterLSDnumber();
	
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	int descriptor;
	vector<int> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor);
	}
	double compare(int ID){
		return (double)abs(descriptor - descriptors[ID]);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDnumber)
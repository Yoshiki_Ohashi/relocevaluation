#include "FilterRFwithout.h"
using namespace cv;


FilterRFwithout::FilterRFwithout() : Filter() {

}

FilterRFwithout::FilterRFwithout(int thread) : Filter(thread) {

}

FilterRFwithout::FilterRFwithout(Method method) : Filter(method) {

}

FilterRFwithout::FilterRFwithout(int thread, Method method) : Filter(thread, method) {

}

FilterRFwithout::FilterRFwithout(Size size) : Filter(size) {

}

FilterRFwithout::FilterRFwithout(int thread, Size size) : Filter(thread, size) {

}

FilterRFwithout::FilterRFwithout(Method method, Size size) : Filter(method, size) {

}

FilterRFwithout::FilterRFwithout(int thread, Method method, Size size) : Filter(thread, method, size) {

}

FilterRFwithout::FilterRFwithout(filter filter) : Filter(filter) {

}

FilterRFwithout::FilterRFwithout(int thread, filter filter) : Filter(thread, filter) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO) : Filter(imgIO) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, int thread) : Filter(imgIO, thread) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, Method method) : Filter(imgIO, method) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, Size size) : Filter(imgIO, size) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, filter filter) : Filter(imgIO, filter) {

}

FilterRFwithout::FilterRFwithout(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter) {

}

FilterRFwithout::~FilterRFwithout()
{
	delete[] positions;
	delete[] thresholds;
	delete[] pPositions;
	for (int i = 0; i < filledDB; ++i) {
		delete[] descriptors[i];
	}
}

bool FilterRFwithout::checkSpeed(int repeat) {
	if (!SpeedIsChecked) {
		randomizePosAndThresh();
		allocMat();
		getPointers();
		random_device rnd;
		uniform_int_distribution<> random(0, DBsize - 1);
		makeDB(repeat);
		timePrepare = 0;
		timeCompare = 0;
		for (int i = 0; i < repeat; i++) {
			ImgIO->read(random(rnd));
			screening(repeat, &timePrepare, &timeCompare);
		}
		clearDB();
		timePrepare = timePrepare * 1000 / (double)repeat;
		timeCompare = timeCompare * 1000 / (double)repeat / (double)repeat;
		SpeedIsChecked = true;
	}
	double time = timePrepare + timeCompare*candidateSize;
	if (time < requiredTime) return true;
	else return false;
}

void FilterRFwithout::makeDB() {
	randomizePosAndThresh();
	allocMat();
	getPointers();
	for (int ID = 0; ID < DBsize; ID++) {
		extractFeature(ID);
		addFeatureToDB();
	}
}

void FilterRFwithout::makeDB(int repeat) {
	random_device rnd;
	uniform_int_distribution<> random(0, DBsize - 1);
	int code;
	for (int i = 0; i < repeat; ++i) {
		extractFeature(random(rnd));
		addFeatureToDB();
	}
}

void FilterRFwithout::randomizePosAndThresh() {
	int n = size.width * size.height * 3;
	vector<int> pixels;
	generateUniqueRandomArray(pixels, 4 * numFerns, 0, n - 1);
	for (int i = 0; i < 4 * numFerns; ++i) {
		positions[i] = pixels[i];
	}

	thresholds = new uchar[4 * numFerns];
	random_device rnd;
	uniform_int_distribution<> rand256(0, 255);
	for (int i = 0; i < 4 * numFerns; ++i) {
		thresholds[i] = (uchar)rand256(rnd);
	}
}

void FilterRFwithout::allocMat() {
	blurred = Mat::zeros(size, CV_32FC3);
}

void FilterRFwithout::getPointers() {
	//float* data = (float*)blurred.data;
	for (int i = 0; i < 4 * numFerns; ++i) {
		if(true) pPositions[i] = (float*)blurred.data + positions[i];
	}
}

void FilterRFwithout::calcFeature() {
	filter2D(ImgIO->getResizedRGB(thread), blurred, CV_32F, kernel);
	for (int i = 0; i < numFerns; ++i) {
		descriptor[i] = 0;
		for (int j = 0; j < 4; ++j) {
			if (*(pPositions[4 * i + j]) < thresholds[4 * i + j]) {
				descriptor[i] |= 1 << j;
			}
		}
	}
}

void FilterRFwithout::addFeatureToDB(){
	descriptors[filledDB] = new uchar[numFerns];
	memcpy(descriptors[filledDB], descriptor, numFerns * sizeof(uchar));
	filledDB++;
}

double FilterRFwithout::compare(int ID) {
	int count = numFerns;
	for (int i = 0; i < numFerns; ++i) {
		if (descriptor[i] == descriptors[ID][i]) {
			--count;
		}
	}
	return (double)count;
}

void FilterRFwithout::generateUniqueRandomArray(vector<int>& vec, const int numElements, const int rangeMIN, const int rangeMAX) {
	int margin = 1.2 * numElements;
	vec.clear();
	vec.reserve(margin);
	random_device rand;
	uniform_int_distribution<> distribution(rangeMIN, rangeMAX);
	while (vec.size() < numElements) {
		while (vec.size() < margin) vec.push_back(distribution(rand));
		std::sort(vec.begin(), vec.end());
		auto unique_end = std::unique(vec.begin(), vec.end());

		if (numElements < std::distance(vec.begin(), unique_end)) {
			unique_end = std::next(vec.begin(), numElements);
		}
		vec.erase(unique_end, vec.end());
	}
	for (int i = 0; i < numElements - 1; ++i) {
		uniform_int_distribution<> dist(i, numElements - 1);
		int rnd = dist(rand);
		int tmp = vec[i];
		vec[i] = vec[rnd];
		vec[rnd] = tmp;
	}
}

void FilterRFwithout::init() {
	allocMat();
	getPointers();
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterRFwithout)
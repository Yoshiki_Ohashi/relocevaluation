#include "FilterCCV.h"



using namespace cv;


FilterCCV::FilterCCV() : Filter(){

}

FilterCCV::FilterCCV(int thread) : Filter(thread){

}

FilterCCV::FilterCCV(Method method) : Filter(method){

}

FilterCCV::FilterCCV(int thread, Method method) : Filter(thread, method){

}

FilterCCV::FilterCCV(Size size) : Filter(size){

}

FilterCCV::FilterCCV(int thread, Size size) : Filter(thread, size){

}

FilterCCV::FilterCCV(Method method, Size size) : Filter(method, size){

}

FilterCCV::FilterCCV(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterCCV::FilterCCV(filter filter) : Filter(filter){

}

FilterCCV::FilterCCV(int thread, filter filter) : Filter(thread, filter){

}

FilterCCV::FilterCCV(ImageIO* imgIO) : Filter(imgIO){

}

FilterCCV::FilterCCV(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterCCV::FilterCCV(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterCCV::FilterCCV(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterCCV::FilterCCV(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterCCV::FilterCCV(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterCCV::FilterCCV(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterCCV::FilterCCV(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterCCV::FilterCCV(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterCCV::FilterCCV(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}

FilterCCV::~FilterCCV()
{
}

void FilterCCV::calcFeature(){
	cv::Mat blurred;
	cv::blur(ImgIO->getResizedRGB(thread), blurred, cv::Size(3, 3));

	const int rows = blurred.rows;
	const int cols = blurred.cols;

	vector<label> lut; //{parent, color, count}
	vector<uchar> color;
	lut.reserve(rows*cols / 8);
	int nlab = 1;
	cv::Mat reduced = cv::Mat::zeros(Size(cols, rows), CV_8UC1);
	cv::Mat labeled = cv::Mat::zeros(Size(cols, rows), CV_32SC1);
	Vec3b *ptrv = blurred.ptr<Vec3b>(0);
	uchar *ptru = reduced.ptr<uchar>(0);
	int *ptri = labeled.ptr<int>(0);
	uchar *ptru_prev;
	int *ptri_prev;

	//左上隅の処理
	ptru[0] = ((ptrv[0][0] >> 6) << 4) + ((ptrv[0][1] >> 6) << 2) + ((ptrv[0][2] >> 6) << 0);
	ptri[0] = 0;
	lut.push_back({ 0, ptru[0], 1 });

	//上端の処理
	for (int col = 1; col < cols; ++col){
		ptru[col] = ((ptrv[col][0] >> 6) << 4) + ((ptrv[col][1] >> 6) << 2) + ((ptrv[col][2] >> 6) << 0);
		if (ptru[col] == ptru[col - 1]){
			ptri[col] = ptri[col - 1];
			lut[ptri[col]].count += 1;
		}
		else{
			ptri[col] = nlab;
			lut.push_back({ nlab, ptru[col], 1 });
			++nlab;
		}
	}
	for (int row = 1; row < rows; ++row){
		ptru_prev = ptru;
		ptri_prev = ptri;
		ptrv = blurred.ptr<Vec3b>(row);
		ptru = reduced.ptr<uchar>(row);
		ptri = labeled.ptr<int>(row);

		//左端の処理
		ptru[0] = ((ptrv[0][0] >> 6) << 4) + ((ptrv[0][1] >> 6) << 2) + ((ptrv[0][2] >> 6) << 0);
		if (ptru[0] == ptru_prev[0]){
			ptri[0] = ptri_prev[0];
			lut[ptri[0]].count += 1;
			if (ptru[0] == ptru_prev[1]){
				ptri[0] = link(lut, ptri[0], ptri_prev[1]);
			}
		}
		else{
			if (ptru[0] == ptru_prev[1]){
				ptri[0] = ptri_prev[1];
				lut[ptri[0]].count += 1;
			}
			else{
				ptri[0] = nlab;
				lut.push_back({ nlab, ptru[0], 1 });
				++nlab;
			}
		}

		//内部の処理
		int col = 1;
		for (; col < cols - 1; ++col){
			ptru[col] = ((ptrv[col][0] >> 6) << 4) + ((ptrv[col][1] >> 6) << 2) + ((ptrv[col][2] >> 6) << 0);
			bool flagA = (ptru[col] == ptru[col - 1]); //左
			bool flagB = (ptru[col] == ptru_prev[col - 1]); //左上
			bool flagC = (ptru[col] == ptru_prev[col]); //上
			bool flagD = (ptru[col] == ptru_prev[col + 1]); //右上
			ptri[col] = nlab;
			lut.push_back({ nlab, ptru[0], 1 });
			if (flagA | flagB | flagC | flagD){
				if (flagA){
					ptri[col] = ptri[col - 1];
					lut[ptri[col]].count += 1;
				}
				if (flagB) ptri[col] = link(lut, ptri[col], ptri_prev[col - 1]);
				if (flagC) ptri[col] = link(lut, ptri[col], ptri_prev[col]);
				if (flagD) ptri[col] = link(lut, ptri[col], ptri_prev[col + 1]);
				lut.pop_back();
			}
			else ++nlab;
		}

		//右端の処理
		ptru[col] = ((ptrv[col][0] >> 6) << 4) + ((ptrv[col][1] >> 6) << 2) + ((ptrv[col][2] >> 6) << 0);
		bool flagA = (ptru[col] == ptru[col - 1]); //左
		bool flagB = (ptru[col] == ptru_prev[col - 1]); //左上
		bool flagC = (ptru[col] == ptru_prev[col]); //上
		ptri[col] = nlab;
		lut.push_back({ nlab, ptru[0], 1 });
		if (flagA | flagB | flagC){
			if (flagA){
				ptri[col] = ptri[col - 1];
				lut[ptri[col]].count += 1;
			}
			if (flagB) ptri[col] = link(lut, ptri[col], ptri_prev[col - 1]);
			if (flagC) ptri[col] = link(lut, ptri[col], ptri_prev[col]);
			lut.pop_back();
		}
		else ++nlab;
	}

	descriptor = Mat::zeros(Size(128, 1), CV_32S);
	const int thresh = blurred.cols*blurred.rows / 100;
	for (int i = 0; i < nlab; ++i){
		if (i == lut[i].parent){
			if (lut[i].count > thresh){
				descriptor.at<int>(0, 2 * lut[i].color) += lut[i].count;
			}
			else{
				descriptor.at<int>(0, 2 * lut[i].color + 1) += lut[i].count;
			}
		}
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterCCV)

inline int compress(vector<label> &lut, int a)
{
	while (a != lut[a].parent)
	{
		lut[a].parent = lut[lut[a].parent].parent;
		
		a = lut[a].parent;
	}
	return a;
}

inline int link(vector<label> &lut, int a, int b)
{
	a = compress(lut, a);
	b = compress(lut, b);
	if (a == b){
		return a;
	}
	else if (a < b){
		lut[b].parent = a;
		lut[a].count += lut[b].count;
		return a;
	}
	else{
		lut[a].parent = b;
		lut[b].count += lut[a].count;
		return b;
	}
}
#pragma once
#include "Filter.h"

class FilterRFwithout :
	public Filter
{
public:
	FilterRFwithout();
	FilterRFwithout(int);
	FilterRFwithout(Method);
	FilterRFwithout(int, Method);
	FilterRFwithout(cv::Size);
	FilterRFwithout(int, cv::Size);
	FilterRFwithout(Method, cv::Size);
	FilterRFwithout(int, Method, cv::Size);
	FilterRFwithout(filter);
	FilterRFwithout(int, filter);
	FilterRFwithout(ImageIO* imgIO);
	FilterRFwithout(ImageIO* imgIO, int);
	FilterRFwithout(ImageIO* imgIO, Method);
	FilterRFwithout(ImageIO* imgIO, int, Method);
	FilterRFwithout(ImageIO* imgIO, cv::Size);
	FilterRFwithout(ImageIO* imgIO, int, cv::Size);
	FilterRFwithout(ImageIO* imgIO, Method, cv::Size);
	FilterRFwithout(ImageIO* imgIO, int, Method, cv::Size);
	FilterRFwithout(ImageIO* imgIO, filter);
	FilterRFwithout(ImageIO* imgIO, int, filter);
	~FilterRFwithout();
	bool checkSpeed(int);
	void makeDB();
	int getDBsize() {
		return filledDB;
	}
	void init();

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version) {
		ar & boost::serialization::base_object<Filter>(*this);
		for (int i = 0; i < numFerns; ++i) {
			for (int j = 0; j < 4; ++j) {
				ar & positions[4 * i + j];
				ar & thresholds[4 * i + j];
			}
		}
		ar & filledDB;
		for (int i = 0; i < filledDB; ++i) {
			if (descriptors[i] == NULL) descriptors[i] = new uchar[numFerns];
			for (int j = 0; j < numFerns; ++j) {
				ar & descriptors[i][j];
			}
		}
	}

	const cv::Mat kernel = cv::getGaussianKernel(15, 2.5, CV_64F); //sigma=2.5, ksizeは2*3*sigmaに近い奇数（正規分布では全体の99.73%が-3sigmaから+3sigmaに含まれる）
	cv::Mat blurred;

	uchar* descriptor = new uchar[numFerns];
	int* positions = new int[4 * numFerns];
	uchar* thresholds = new uchar[4 * numFerns];
	float** pPositions = new float*[4 * numFerns];

	void randomizePosAndThresh();
	void allocMat();
	void getPointers();

	void makeDB(int);
	void clearDB() {
		for (int i = 0; i < filledDB; ++i) {
			delete[] descriptors[i];
		}
		filledDB = 0;
	}

	uchar** descriptors = new uchar*[DBsize]();
	int filledDB = 0;

	void extractFeature(int ID) {
		extractFeatureFromRGB(ID);
	}
	void extractFeature() {
		extractFeatureFromRGB();
	}
	void calcFeature();
	void addFeatureToDB();
	double compare(int);
	void generateUniqueRandomArray(vector<int>&, const int, const int, const int);
};

BOOST_CLASS_EXPORT_KEY(FilterRFwithout)
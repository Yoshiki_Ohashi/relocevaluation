#include "FilterLBPhist.h"

using namespace cv;


FilterLBPhist::FilterLBPhist() : Filter(){

}

FilterLBPhist::FilterLBPhist(int thread) : Filter(thread){

}

FilterLBPhist::FilterLBPhist(Method method) : Filter(method){

}

FilterLBPhist::FilterLBPhist(int thread, Method method) : Filter(thread, method){

}

FilterLBPhist::FilterLBPhist(Size size) : Filter(size){

}

FilterLBPhist::FilterLBPhist(int thread, Size size) : Filter(thread, size){

}

FilterLBPhist::FilterLBPhist(Method method, Size size) : Filter(method, size){

}

FilterLBPhist::FilterLBPhist(int thread, Method method, Size size) : Filter(thread, method, size){

}

FilterLBPhist::FilterLBPhist(filter filter) : Filter(filter){

}

FilterLBPhist::FilterLBPhist(int thread, filter filter) : Filter(thread, filter){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO) : Filter(imgIO){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, int thread) : Filter(imgIO, thread){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, Method method) : Filter(imgIO, method){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, Size size) : Filter(imgIO, size){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){

}

FilterLBPhist::FilterLBPhist(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){

}


FilterLBPhist::~FilterLBPhist()
{
}

void FilterLBPhist::calcFeature(){
	Mat src = ImgIO->getResizedGRAY(thread);
	Mat dst = Mat::zeros(src.rows - 2, src.cols - 2, CV_8UC1);
	for (int i = 1; i<src.rows - 1; i++) {
		for (int j = 1; j<src.cols - 1; j++) {
			uchar center = src.at<uchar>(i, j);
			uchar code = 0;
			code |= (src.at<uchar>(i - 1, j - 1) > center) << 7;
			code |= (src.at<uchar>(i - 1, j) > center) << 6;
			code |= (src.at<uchar>(i - 1, j + 1) > center) << 5;
			code |= (src.at<uchar>(i, j + 1) > center) << 4;
			code |= (src.at<uchar>(i + 1, j + 1) > center) << 3;
			code |= (src.at<uchar>(i + 1, j) > center) << 2;
			code |= (src.at<uchar>(i + 1, j - 1) > center) << 1;
			code |= (src.at<uchar>(i, j - 1) > center) << 0;
			dst.at<uchar>(i - 1, j - 1) = code;
		}
	}
	int histSize[] = { 256 };
	float range[] = { 0, 256 };
	const float* ranges[] = { range };
	int channels[] = { 0 };
	calcHist(&dst, 1, channels, Mat(), descriptor, 1, histSize, ranges, true, false);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLBPhist)
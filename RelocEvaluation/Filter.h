#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <cfloat>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/ximgproc.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/thread.hpp>
#include <windows.h>
#include "cvmat_serilization.h"
#include "ImageIO.h"
#include "define.h"

enum Method
{
	RGBval,
	GRAYval,
	RGBhist,
	GRAYhist,
	LBPhist,
	HOG,
	CCV,
	Gabor,
	SURF,
	AKAZE,
	LSDnumber,
	LSDlength,
	LSDregion,
	LSDparallel,
	LSDdistance,
	LSDangle,
	LSDsolo,
	LSDcombi,
	RF,
	RForiginal,
	RFwithout,
	GRAYvalNCC,
	RGBvalNCC,
	AKAZEmatch,
	SURFmatch,
};

struct filter
{
	Method method;
	cv::Size size;
	double thresh = 0;
	bool SpeedIsChecked = false;
	double timePrepare;
	double timeCompare;
	double AvgPassed;
	int NumPrevious;

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & method;
		ar & size.width;
		ar & size.height;
		ar & thresh;
		ar & SpeedIsChecked;
		ar & timePrepare;
		ar & timeCompare;
		ar & AvgPassed;
		ar & NumPrevious;
	}
};

struct result
{
	int ID;
	double distance;
	bool isCorrect;
	int accumulated;
};

class Filter
{
public:
	Filter();
	Filter(int);
	Filter(Method);
	Filter(int, Method);
	Filter(cv::Size);
	Filter(int, cv::Size);
	Filter(Method, cv::Size);
	Filter(int, Method, cv::Size);
	Filter(filter);
	Filter(int, filter);
	Filter(ImageIO* imgIO);
	Filter(ImageIO* imgIO, int);
	Filter(ImageIO* imgIO, Method);
	Filter(ImageIO* imgIO, int, Method);
	Filter(ImageIO* imgIO, cv::Size);
	Filter(ImageIO* imgIO, int, cv::Size);
	Filter(ImageIO* imgIO, Method, cv::Size);
	Filter(ImageIO* imgIO, int, Method, cv::Size);
	Filter(ImageIO* imgIO, filter);
	Filter(ImageIO* imgIO, int, filter);
	virtual ~Filter() = 0;

	bool SpeedIsChecked;
	int requiredTime; //ms
	double timePrepare; //ms
	double timeCompare; //ms
	volatile bool isFinished;
	static boost::mutex sync[NumThreads - 1];

	void setImageIO(ImageIO* imgIO){
		ImgIO = imgIO;
	}
	void setThreadNum(int);
	void setMethod(Method method){
		this->method = method;
	}
	Method getMethod(){
		return method;
	}
	virtual void setSize(cv::Size size) {
		this->size = size;
	}
	cv::Size getSize(){
		return size;
	}
	void setThresh(double thresh){
		this->thresh = thresh;
	}
	double getThresh(){
		return thresh;
	}
	void setFilter(filter filter){
		setMethod(filter.method);
		setSize(filter.size);
		setThresh(filter.thresh);
		SpeedIsChecked = filter.SpeedIsChecked;
		timePrepare = filter.timePrepare;
		timeCompare = filter.timeCompare;
	}
	void setTime(int time){
		requiredTime = time;
	}
	void setCandidateSize(double size){
		candidateSize = size;
	}
	virtual int getDBsize() = 0;
	virtual bool checkSpeed(int);
	virtual void makeDB();
	double screening(vector<double>*, vector<double>*, int, int);
	void screeningF(vector<int>*, vector<int>*);
	void screeningL(vector<int>*, vector<int>*, volatile bool* prev);
	void screeningFL(vector<int>*, vector<int>*);
	void screeningN(vector<int>*, vector<int>*, volatile bool* prev);

	Filter &operator=(const filter other){
		method = other.method;
		this->setSize(other.size);
		thresh = other.thresh;
		SpeedIsChecked = other.SpeedIsChecked;
		timePrepare = other.timePrepare;
		timeCompare = other.timeCompare;
		return *this;
	}

	virtual void init() {}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){

	}

protected:
	virtual void makeDB(int);
	virtual void clearDB() = 0;
	void screening(int, double*, double*);

	ImageIO* ImgIO;
	int thread;
	Method method;
	cv::Size size;
	double thresh;
	
	bool isFirstFilter;
	bool isLastFilter;
	double candidateSize;

	boost::mutex::scoped_lock* pLockPrev;
	boost::mutex::scoped_lock* pLockNext;

	virtual void extractFeature(int) = 0;
	void extractFeatureFromRGB(int ID){
		ImgIO->read(ID, RGB, size, thread);
		calcFeature();
	}
	void extractFeatureFromGRAY(int ID){
		ImgIO->read(ID, GRAY, size, thread);
		calcFeature();
	}
	void extractFeatureFromDEPTH(int ID){
		ImgIO->read(ID, DEPTH, size, thread);
		calcFeature();
	}
	void extractFeatureFromBOTH(int ID){
		ImgIO->read(ID, RGB, size, thread);
		ImgIO->read(ID, DEPTH, size, thread);
		calcFeature();
	}
	void extractFeatureFromALL(int ID){
		ImgIO->read(ID, size, thread);
		calcFeature();
	}
	virtual void extractFeature() = 0;
	void extractFeatureFromRGB(){
		ImgIO->read(RGB, size, thread);
		calcFeature();
	}
	void extractFeatureFromGRAY(){
		ImgIO->read(GRAY, size, thread);
		calcFeature();
	}
	void extractFeatureFromDEPTH(){
		ImgIO->read(DEPTH, size, thread);
		calcFeature();
	}
	void extractFeatureFromBOTH(){
		ImgIO->read(RGB, size, thread);
		ImgIO->read(DEPTH, size, thread);
		calcFeature();
	}
	void extractFeatureFromALL(){
		ImgIO->read(size, thread);
		calcFeature();
	}
	virtual void calcFeature() = 0;
	virtual void addFeatureToDB() = 0;
	bool compareWithIDcheck(int ID, double* distance);
	virtual double compare(int ID) = 0;
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Filter)

string nameMethod(Method);
string nameSize(cv::Size);

void copy(filter& filter, Filter* other);

bool compareKeypoints(const cv::KeyPoint&, const cv::KeyPoint&);

bool compareResult(const result&, const result&);

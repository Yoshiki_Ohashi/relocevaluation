#pragma once
#include "Filter.h"
#include <vector>
class FilterCCV :
	public Filter
{
public:
	FilterCCV();
	FilterCCV(int);
	FilterCCV(Method);
	FilterCCV(int, Method);
	FilterCCV(cv::Size);
	FilterCCV(int, cv::Size);
	FilterCCV(Method, cv::Size);
	FilterCCV(int, Method, cv::Size);
	FilterCCV(filter);
	FilterCCV(int, filter);
	FilterCCV(ImageIO* imgIO);
	FilterCCV(ImageIO* imgIO, int);
	FilterCCV(ImageIO* imgIO, Method);
	FilterCCV(ImageIO* imgIO, int, Method);
	FilterCCV(ImageIO* imgIO, cv::Size);
	FilterCCV(ImageIO* imgIO, int, cv::Size);
	FilterCCV(ImageIO* imgIO, Method, cv::Size);
	FilterCCV(ImageIO* imgIO, int, Method, cv::Size);
	FilterCCV(ImageIO* imgIO, filter);
	FilterCCV(ImageIO* imgIO, int, filter);
	~FilterCCV();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromRGB(ID);
	}
	void extractFeature(){
		extractFeatureFromRGB();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterCCV)

typedef struct{
	int parent;
	uchar color;
	int count;
}label;

inline int compress(vector<label> &lut, int a);
inline int link(vector<label> &lut, int a, int b);
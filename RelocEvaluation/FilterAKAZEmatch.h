#pragma once
#include "Filter.h"
#include "BOWKmajorityTrainer.h"

class FilterAKAZEmatch :
	public Filter
{
public:
	FilterAKAZEmatch();
	FilterAKAZEmatch(int);
	FilterAKAZEmatch(Method);
	FilterAKAZEmatch(int, Method);
	FilterAKAZEmatch(cv::Size);
	FilterAKAZEmatch(int, cv::Size);
	FilterAKAZEmatch(Method, cv::Size);
	FilterAKAZEmatch(int, Method, cv::Size);
	FilterAKAZEmatch(filter);
	FilterAKAZEmatch(int, filter);
	FilterAKAZEmatch(ImageIO* imgIO);
	FilterAKAZEmatch(ImageIO* imgIO, int);
	FilterAKAZEmatch(ImageIO* imgIO, Method);
	FilterAKAZEmatch(ImageIO* imgIO, int, Method);
	FilterAKAZEmatch(ImageIO* imgIO, cv::Size);
	FilterAKAZEmatch(ImageIO* imgIO, int, cv::Size);
	FilterAKAZEmatch(ImageIO* imgIO, Method, cv::Size);
	FilterAKAZEmatch(ImageIO* imgIO, int, Method, cv::Size);
	FilterAKAZEmatch(ImageIO* imgIO, filter);
	FilterAKAZEmatch(ImageIO* imgIO, int, filter);
	~FilterAKAZEmatch();

	int getDBsize() {
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version) {
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
		ar & vecKeypoints;
	}

	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	vector<cv::KeyPoint> keypoints;
	vector<vector<cv::KeyPoint>> vecKeypoints;
	cv::Ptr<cv::FeatureDetector> detector = cv::AKAZE::create();
	cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");

	void clearDB() {
		descriptors.clear();
	}
	void extractFeature(int ID) {
		extractFeatureFromGRAY(ID);
	}
	void extractFeature() {
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB() {
		descriptors.push_back(descriptor.clone());
		vecKeypoints.push_back(keypoints);
	}
	double compare(int ID);
};

BOOST_CLASS_EXPORT_KEY(FilterAKAZEmatch)
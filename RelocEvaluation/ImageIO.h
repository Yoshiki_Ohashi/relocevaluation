#pragma once

#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
#include "define.h"

using namespace std;

enum imgType{
	RGB,
	GRAY,
	DEPTH,
};

class ImageIO
{
public:
	ImageIO();
	ImageIO(string);
	~ImageIO();
	void read(int);
	void read(int, imgType);
	void read(int, cv::Size, int);
	void read(int, imgType, cv::Size, int);
	void read(cv::Size, int);
	void read(imgType, cv::Size, int);
	void write();
	void write(imgType);
	void write(imgType, int);
	
	void setDBpath(string path){
		DBpath = path;
	}
	string getDBpath(){
		return DBpath;
	}
	cv::Mat getRGB(){
		boost::mutex::scoped_lock lock(sync);
		return rgb.clone();
	}
	cv::Mat getGRAY(){
		boost::mutex::scoped_lock lock(sync);
		return gray.clone();
	}
	cv::Mat getDEPTH(){
		boost::mutex::scoped_lock lock(sync);
		return depth.clone();
	}
	cv::Mat getResizedRGB(int thread){
		return resizedRGB[thread].clone();
	}
	cv::Mat getResizedGRAY(int thread){
		return resizedGRAY[thread].clone();
	}
	cv::Mat getResizedDEPTH(int thread){
		return resizedDEPTH[thread].clone();
	}
	int getID(){
		if (ID_RGB == ID_GRAY && ID_RGB == ID_DEPTH){
			return ID_RGB;
		}
		else return -1;
	}

private:
	string DBpath;
	int ID_RGB;
	int ID_GRAY;
	int ID_DEPTH;
	cv::Mat rgb;
	cv::Mat gray;
	cv::Mat depth;
	cv::Mat resizedRGB[NumThreads];
	cv::Mat resizedGRAY[NumThreads];
	cv::Mat resizedDEPTH[NumThreads];
	boost::mutex sync;
};


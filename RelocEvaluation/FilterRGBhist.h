#pragma once
#include "Filter.h"
class FilterRGBhist :
	public Filter
{
public:
	FilterRGBhist();
	FilterRGBhist(int);
	FilterRGBhist(Method);
	FilterRGBhist(int, Method);
	FilterRGBhist(cv::Size);
	FilterRGBhist(int, cv::Size);
	FilterRGBhist(Method, cv::Size);
	FilterRGBhist(int, Method, cv::Size);
	FilterRGBhist(filter);
	FilterRGBhist(int, filter);
	FilterRGBhist(ImageIO* imgIO);
	FilterRGBhist(ImageIO* imgIO, int);
	FilterRGBhist(ImageIO* imgIO, Method);
	FilterRGBhist(ImageIO* imgIO, int, Method);
	FilterRGBhist(ImageIO* imgIO, cv::Size);
	FilterRGBhist(ImageIO* imgIO, int, cv::Size);
	FilterRGBhist(ImageIO* imgIO, Method, cv::Size);
	FilterRGBhist(ImageIO* imgIO, int, Method, cv::Size);
	FilterRGBhist(ImageIO* imgIO, filter);
	FilterRGBhist(ImageIO* imgIO, int, filter);
	~FilterRGBhist();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors; 
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromRGB(ID);
	}
	void extractFeature(){
		extractFeatureFromRGB();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterRGBhist)
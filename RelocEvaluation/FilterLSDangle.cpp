#include "FilterLSDangle.h"

using namespace cv;


FilterLSDangle::FilterLSDangle() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDangle::FilterLSDangle(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDangle::~FilterLSDangle()
{
}

void FilterLSDangle::calcFeature(){
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = cv::Mat::zeros(cv::Size(4, 1), CV_32S);
	int* des = descriptor.ptr<int>(0);
	Vec2f veca, vecb;
	float norma, normb;
	double angle;
	for (int i = 0; i < (int)lines.size() - 1; ++i){
		veca[0] = lines[i][0] - lines[i][2];
		veca[1] = lines[i][1] - lines[i][3];
		norma = veca[0] * veca[0] + veca[1] * veca[1];
		for (int j = i + 1; j < (int)lines.size(); ++j) {
			vecb[0] = lines[j][0] - lines[j][2];
			vecb[1] = lines[j][1] - lines[j][3];
			normb = vecb[0] * vecb[0] + vecb[1] * vecb[1];
			angle = (veca[0] * vecb[0] + veca[1] * vecb[1])*(veca[0] * vecb[0] + veca[1] * vecb[1]) / norma / normb;
			if (angle > th2) {
				if (angle > th1) des[0] += 1;
				else des[1] += 1;
			}
			else {
				if (angle > th3) des[2] += 1;
				else des[3] += 1;
			}
		}
	}
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDangle)
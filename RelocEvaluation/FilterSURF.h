#pragma once
#include "Filter.h"

class FilterSURF :
	public Filter
{
public:
	FilterSURF();
	FilterSURF(int);
	FilterSURF(Method);
	FilterSURF(int, Method);
	FilterSURF(cv::Size);
	FilterSURF(int, cv::Size);
	FilterSURF(Method, cv::Size);
	FilterSURF(int, Method, cv::Size);
	FilterSURF(filter);
	FilterSURF(int, filter);
	FilterSURF(ImageIO* imgIO);
	FilterSURF(ImageIO* imgIO, int);
	FilterSURF(ImageIO* imgIO, Method);
	FilterSURF(ImageIO* imgIO, int, Method);
	FilterSURF(ImageIO* imgIO, cv::Size);
	FilterSURF(ImageIO* imgIO, int, cv::Size);
	FilterSURF(ImageIO* imgIO, Method, cv::Size);
	FilterSURF(ImageIO* imgIO, int, Method, cv::Size);
	FilterSURF(ImageIO* imgIO, filter);
	FilterSURF(ImageIO* imgIO, int, filter);
	~FilterSURF();

	bool checkSpeed(int);
	void makeDB();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
		ar & vocabulary;
		ar & hasVocabulary;
		ar & hasDB;
		if (hasVocabulary) extractor->setVocabulary(vocabulary);
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;
	cv::Mat vocabulary;
	cv::Ptr<cv::FeatureDetector> detector = cv::xfeatures2d::SURF::create();
	cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create("BruteForce");
	vector<cv::KeyPoint> keypoints;
	cv::Ptr<cv::BOWImgDescriptorExtractor> extractor = new cv::BOWImgDescriptorExtractor(detector, matcher);

	bool hasVocabulary = false;
	bool hasDB = false;

	void makeVocabulary();
	void makeDB(int repeat);
	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterSURF)

#include "FilterSURF.h"

using namespace cv;


FilterSURF::FilterSURF() : Filter(){
	
	
}

FilterSURF::FilterSURF(int thread) : Filter(thread){
	
	
}

FilterSURF::FilterSURF(Method method) : Filter(method){
	
	
}

FilterSURF::FilterSURF(int thread, Method method) : Filter(thread, method){
	
	
}

FilterSURF::FilterSURF(Size size) : Filter(size){
	
	
}

FilterSURF::FilterSURF(int thread, Size size) : Filter(thread, size){
	
	
}

FilterSURF::FilterSURF(Method method, Size size) : Filter(method, size){
	
	
}

FilterSURF::FilterSURF(int thread, Method method, Size size) : Filter(thread, method, size){
	
	
}

FilterSURF::FilterSURF(filter filter) : Filter(filter){
	
	
}

FilterSURF::FilterSURF(int thread, filter filter) : Filter(thread, filter){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO) : Filter(imgIO){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	
	
}

FilterSURF::FilterSURF(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	
	
}

FilterSURF::~FilterSURF()
{
}

bool FilterSURF::checkSpeed(int repeat){
	if (!SpeedIsChecked){
		if (!hasVocabulary) {
			makeVocabulary();
			extractor->setVocabulary(vocabulary);
		}
		random_device rnd;
		uniform_int_distribution<> random(0, DBsize - 1);
		if(!hasDB) makeDB(repeat);
		timePrepare = 0;
		timeCompare = 0;
		for (int i = 0; i < repeat; i++){
			ImgIO->read(random(rnd));
			screening(repeat, &timePrepare, &timeCompare);
		}
		if(!hasDB) clearDB();
		timePrepare = timePrepare * 1000 / (double)repeat;
		timeCompare = timeCompare * 1000 / (double)repeat / (double)repeat;
		SpeedIsChecked = true;
	}
	double time = timePrepare + timeCompare*candidateSize;
	if (time < requiredTime) return true;
	else return false;
}

void FilterSURF::makeDB(){
	if (!hasVocabulary) {
		makeVocabulary();
		extractor->setVocabulary(vocabulary);
	}

	if (!hasDB){
		for (int ID = 0; ID < DBsize; ID++){
			extractFeature(ID);
			addFeatureToDB();
		}
		hasDB = true;
	}
}

void FilterSURF::makeDB(int repeat){
	random_device rnd;
	uniform_int_distribution<> random(0, DBsize - 1);
	for (int i = 0; i < repeat; i++){
		extractFeature(random(rnd));
		addFeatureToDB();
	}
}

void FilterSURF::makeVocabulary(){
	const int increment = DBsize / NumTrainingImages;
	cv::BOWKMeansTrainer trainer(DictionarySize, cv::TermCriteria(CV_TERMCRIT_ITER, 100, 0.001), 3, KMEANS_PP_CENTERS);

	for (int ID = 0; ID < DBsize; ID += increment){
		ImgIO->read(ID, GRAY, size, thread);
		Mat src = ImgIO->getResizedGRAY(thread);
		detector->detect(src, keypoints);
		if (keypoints.size() > 0) {
			if (keypoints.size() > 100) {
				std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);
				keypoints.erase(keypoints.begin() + 100, keypoints.end());
			}
			detector->compute(src, keypoints, descriptor);
			trainer.add(descriptor);
		}
	}

	vocabulary = trainer.cluster();
	hasVocabulary = true;
}

void FilterSURF::calcFeature(){
	Mat src = ImgIO->getResizedGRAY(thread);
	detector->detect(src, keypoints);
	if (keypoints.size() > 500) {
		std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);
		keypoints.erase(keypoints.begin() + 500, keypoints.end());
	}
	if (keypoints.size() == 0) descriptor = Mat::zeros(Size(DictionarySize, 1), CV_32F);
	else extractor->compute(src, keypoints, descriptor);
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterSURF)
#pragma once

#ifdef _DEBUG
#define  CV_EXT_STR "330d.lib"
#else
#define  CV_EXT_STR "330.lib"
#endif

#pragma comment(lib, "opencv_calib3d" CV_EXT_STR)
//#pragma comment(lib, "opencv_contrib" CV_EXT_STR)
#pragma comment(lib, "opencv_core" CV_EXT_STR)
#pragma comment(lib, "opencv_features2d" CV_EXT_STR)
//#pragma comment(lib, "opencv_flann" CV_EXT_STR)
//#pragma comment(lib, "opencv_gpu" CV_EXT_STR)
#pragma comment(lib, "opencv_highgui" CV_EXT_STR)
#pragma comment(lib, "opencv_imgproc" CV_EXT_STR)
#pragma comment(lib, "opencv_imgcodecs" CV_EXT_STR)
//#pragma comment(lib, "opencv_legacy" CV_EXT_STR)
//#pragma comment(lib, "opencv_ml" CV_EXT_STR)
//#pragma comment(lib, "opencv_nonfree" CV_EXT_STR)
//#pragma comment(lib, "opencv_objdetect" CV_EXT_STR)
//#pragma comment(lib, "opencv_ocl" CV_EXT_STR)
//#pragma comment(lib, "opencv_photo" CV_EXT_STR)
//#pragma comment(lib, "opencv_stitching" CV_EXT_STR)
//#pragma comment(lib, "opencv_superres" CV_EXT_STR)
//#pragma comment(lib, "opencv_line_descriptor" CV_EXT_STR)
#pragma comment(lib, "opencv_videoio" CV_EXT_STR)
//#pragma comment(lib, "opencv_videostab" CV_EXT_STR)
#pragma comment(lib, "opencv_ximgproc" CV_EXT_STR)
#pragma comment(lib, "opencv_xfeatures2d" CV_EXT_STR)

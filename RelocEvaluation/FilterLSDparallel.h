#pragma once
#include "Filter.h"

class FilterLSDparallel :
	public Filter
{
public:
	FilterLSDparallel();
	FilterLSDparallel(int);
	FilterLSDparallel(Method);
	FilterLSDparallel(int, Method);
	FilterLSDparallel(cv::Size);
	FilterLSDparallel(int, cv::Size);
	FilterLSDparallel(Method, cv::Size);
	FilterLSDparallel(int, Method, cv::Size);
	FilterLSDparallel(filter);
	FilterLSDparallel(int, filter);
	FilterLSDparallel(ImageIO* imgIO);
	FilterLSDparallel(ImageIO* imgIO, int);
	FilterLSDparallel(ImageIO* imgIO, Method);
	FilterLSDparallel(ImageIO* imgIO, int, Method);
	FilterLSDparallel(ImageIO* imgIO, cv::Size);
	FilterLSDparallel(ImageIO* imgIO, int, cv::Size);
	FilterLSDparallel(ImageIO* imgIO, Method, cv::Size);
	FilterLSDparallel(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDparallel(ImageIO* imgIO, filter);
	FilterLSDparallel(ImageIO* imgIO, int, filter);
	~FilterLSDparallel();
	int getDBsize(){
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	const float horizontal_th = tanf(CV_PI / 18.);
	const float vertical_th = tanf(CV_PI*4. / 9.);

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDparallel)
#include "FilterLSDnumber.h"

using namespace cv;


FilterLSDnumber::FilterLSDnumber() : Filter(){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(int thread) : Filter(thread){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(Method method) : Filter(method){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(int thread, Method method) : Filter(thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(Size size) : Filter(size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(int thread, Size size) : Filter(thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(Method method, Size size) : Filter(method, size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(int thread, Method method, Size size) : Filter(thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(filter filter) : Filter(filter){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(int thread, filter filter) : Filter(thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO) : Filter(imgIO){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, int thread) : Filter(imgIO, thread){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, Method method) : Filter(imgIO, method){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, int thread, Method method) : Filter(imgIO, thread, method){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, Size size) : Filter(imgIO, size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, int thread, Size size) : Filter(imgIO, thread, size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, Method method, Size size) : Filter(imgIO, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, int thread, Method method, Size size) : Filter(imgIO, thread, method, size){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, filter filter) : Filter(imgIO, filter){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::FilterLSDnumber(ImageIO* imgIO, int thread, filter filter) : Filter(imgIO, thread, filter){
	detector = createLineSegmentDetector();
}

FilterLSDnumber::~FilterLSDnumber()
{
}

void FilterLSDnumber::calcFeature(){
	detector->detect(ImgIO->getResizedGRAY(thread), lines);
	descriptor = (int)lines.size();
}

BOOST_CLASS_EXPORT_IMPLEMENT(FilterLSDnumber)
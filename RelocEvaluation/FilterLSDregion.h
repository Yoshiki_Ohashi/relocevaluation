#pragma once
#include "Filter.h"

class FilterLSDregion :
	public Filter
{
public:
	FilterLSDregion();
	FilterLSDregion(int);
	FilterLSDregion(Method);
	FilterLSDregion(int, Method);
	FilterLSDregion(cv::Size);
	FilterLSDregion(int, cv::Size);
	FilterLSDregion(Method, cv::Size);
	FilterLSDregion(int, Method, cv::Size);
	FilterLSDregion(filter);
	FilterLSDregion(int, filter);
	FilterLSDregion(ImageIO* imgIO);
	FilterLSDregion(ImageIO* imgIO, int);
	FilterLSDregion(ImageIO* imgIO, Method);
	FilterLSDregion(ImageIO* imgIO, int, Method);
	FilterLSDregion(ImageIO* imgIO, cv::Size);
	FilterLSDregion(ImageIO* imgIO, int, cv::Size);
	FilterLSDregion(ImageIO* imgIO, Method, cv::Size);
	FilterLSDregion(ImageIO* imgIO, int, Method, cv::Size);
	FilterLSDregion(ImageIO* imgIO, filter);
	FilterLSDregion(ImageIO* imgIO, int, filter);
	~FilterLSDregion();
	
	int getDBsize() {
		return (int)descriptors.size();
	}

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version){
		ar & boost::serialization::base_object<Filter>(*this);
		ar & descriptors;
	}
	cv::Mat descriptor;
	vector<cv::Mat> descriptors;

	cv::Ptr<cv::LineSegmentDetector> detector;
	vector<cv::Vec4f> lines;

	void clearDB(){
		descriptors.clear();
	}
	void extractFeature(int ID){
		extractFeatureFromGRAY(ID);
	}
	void extractFeature(){
		extractFeatureFromGRAY();
	}
	void calcFeature();
	void addFeatureToDB(){
		descriptors.push_back(descriptor.clone());
	}
	double compare(int ID){
		return norm(descriptor, descriptors[ID], cv::NORM_L2);
	}
};

BOOST_CLASS_EXPORT_KEY(FilterLSDregion)